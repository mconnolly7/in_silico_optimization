function [pos_state, stim_params, optimal_est] ...
    = spsa_opt(objective_model, n_trials, n_samples, params)

for c1 = 1:n_trials
    [pos_state(:,c1), stim_params(:,:,c1), optimal_est(:,:,c1)] = spsa_optimization(objective_model, 'maximize', n_samples, params);
end

end

function [pos_state, stim_params, optimal_est] ...
    = spsa_optimization( objective_model, direction, n_samples, params )

lower       = objective_model.lower_bound;
upper       = objective_model.upper_bound;

m           = (upper - lower)/2;
b           = (upper + lower)/2;
p           = size(lower,2);

theta   = [];
for c1 = 1:p
    theta(c1) = unifrnd(lower(c1),upper(c1));
end

n           = floor(n_samples / 3);
a           = params(1);
A           = 10;
c           = params(2);
alpha       = 0.602;
gamma       = 2;

stim_params = theta;
for c1=1:n
    ak          = a/(c1+A)^alpha;
    ck          = c/c1^gamma;
    
    y(c1)       = objective_model.sample(theta(c1,:));
    
    delta       = 2*round(rand(1,p)) - 1;
    
    theta_plus  = theta(c1,:) + ck*delta;
    theta_minus = theta(c1,:) - ck*delta;
    
    theta_plus  = min(theta_plus, upper);
    theta_plus  = max(theta_plus, lower);
    
    theta_minus = min(theta_minus, upper);
    theta_minus = max(theta_minus, lower);
    
    yplus        = objective_model.sample(theta_plus);
    yminus       = objective_model.sample(theta_minus);
    
    if strcmp(direction, 'maximize')
        yplus               = -1*yplus;
        yminus              = -1*yminus;
        
        [max_y, max_y_idx]  = max(y);
        optimal_est(c1,:)   = [c1*3 theta(max_y_idx,:)];
    else
        [min_y, min_y_idx]  = min(y);
        optimal_est(c1,:)   = [c1*3 theta(min_y_idx,:)];
    end
    
    ghat          = (yplus - yminus) ./ (2*ck*delta);
    
    theta(c1+1,:) = theta(c1,:) - ak*ghat;
    
    theta(c1+1,:) = min(theta(c1+1,:), upper);
    theta(c1+1,:) = max(theta(c1+1,:), lower);
    
    stim_params  = [stim_params; theta_plus; theta_minus; theta(c1+1,:)];   

end

% stim_params     = theta(1:end-1,:);
pos_state       = y';

end

