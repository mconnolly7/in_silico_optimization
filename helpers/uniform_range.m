function p = uniform_range(lower, upper, n, seed)

if nargin == 4
    rng(seed)
end

p = (upper - lower)  .* rand(n, size(lower, 2)) + lower;

end

