function [stim_params, policy_est] = sdbo_opt_update(x0, u1, x1, params, lower_bound, upper_bound)

policy_est = simulation_gp();

policy_est.initialize_data(x0, u1, x1, lower_bound, upper_bound)
policy_est.gp_model.minimize(10)

pre_state     = x1(end);
switch params(1)
    case 1
        stim_params   = policy_est.state_aquisition(pre_state, 'PI', params(2));
    case 2
        stim_params   = policy_est.state_aquisition(pre_state, 'EI', params(2));
    case 3
        stim_params   = policy_est.state_aquisition(pre_state, 'UCB', params(2));
end
    
end

