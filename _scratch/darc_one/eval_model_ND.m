function mean_error = eval_model_ND(P, rc_struct, n_trials, PLOT)

% P = W_out_ga;
n_samples           = rc_struct.n_samples;
W_in                = rc_struct.W_in;
W                   = rc_struct.W;
n_params            = rc_struct.n_params;

n_res               = size(W,1);
leak_rate           = P(1);
x_state             = P(2:n_res+1)';

W_out               = P(n_res+2:end);
W_out               = reshape(W_out, n_params, []);

% x_1                 = -4:.01:4;
% x_2                 = x_1;
input_space         = rand(1000,2)*8-4;
% input_space         = combvec(x_1, x_2)';

for c1 = 1:n_trials
    
%     x_state                 = rc_struct.x;

    % Generate a trial-specific objective function
    objective_means         = rand(1,n_params)*8-4;
    objective_max           = objective_function_ND(objective_means, objective_means, 1);
    optimal_param           = objective_means(1,:);
    
    % And scale for magnitude 1
    % TODO add minumum 0
    objective_scale         = 1/objective_max;
    objective_noise         = objective_max * 0.1;
    
    % Randomly generate first sample
    input_param             = rand(1,n_params) * 8 - 4;
    objective_val           = objective_function_noise_ND(input_param, objective_means, objective_noise, objective_scale);
    
    max_val_noise           = objective_val;
    max_param               = input_param;
    max_val                 = objective_function_ND(input_param, objective_means, objective_scale);
    
    for t = 1:n_samples     
        u                   = [input_param(t,:)'; objective_val(t)];
        
        % Get the next parameter
        x_state             = (1-leak_rate) * x_state + leak_rate * tanh(W_in * [1; u] + W * x_state);
        next_param          = W_out * [1; u; x_state];
        next_param          = next_param';
        
        % Normalize
        next_param          = max(next_param, -4);
        next_param          = min(next_param, 4);
        
        input_param(t+1,:) 	= next_param;

        % Apply next input to the objective function
        objective_val(t+1)                  = objective_function_noise_ND(next_param, objective_means, objective_noise, objective_scale);
        
        % Get the max param
        [max_val_noise(t+1), max_val_idx]   = max(objective_val);
        max_param(t+1,:)                    = input_param(max_val_idx,:);
        max_val(t+1)                        = objective_function_ND(max_param(t+1,:), objective_means, objective_scale);
        
        % Get the max param
%         max_param(t+1,:)                    = input_param(end,:);
%         max_val(t+1)                        = objective_function_ND(max_param(t+1,:), objective_means, objective_scale);
        
    end
    
    cummulative_error(c1) = sum(1 - max_val);
    
    if cummulative_error(c1) < 0
       a = 1; 
    end
end

if PLOT
    subplot(3,1,1)
    hold on
    plot(input_param(:,1), 'color', .5*[1 1 1])
    plot(max_param(:,1), 'k')
    plot([1 n_samples], [optimal_param(1) optimal_param(1)], 'k--', 'LineWidth', 2)
    
    subplot(3,1,2)
    hold on
    plot(input_param(:,2), 'color', .5*[1 1 1])
    plot(max_param(:,2), 'k')
    plot([1 n_samples], [optimal_param(2) optimal_param(2)], 'k--', 'LineWidth', 2)
    
    subplot(3,1,3)
    plot(objective_max - max_val)
    title(sprintf('Cumm. Error: %.2f', cummulative_error));
end

mean_error = mean(cummulative_error);
% end