function [pos_state, stim_params, optimal_est] =  sdbo_opt_on_model(dynamics_model, n_samples, params)

lower_bound = dynamics_model.lower_bound;
upper_bound = dynamics_model.upper_bound;

dynamics_model.set_initial_state();

for c1 = 1:size(lower_bound,2)
    p(c1,:)  =  polyfit([0 1], [lower_bound(c1), upper_bound(c1)],1);
end

n_burn_in   = 10;
n_learning  = n_samples - n_burn_in;

for c1 = 1:n_burn_in
    for c2 = 1:size(lower_bound,2)
        stim_params(c1,c2)   = polyval(p(c2,:), rand);
    end
    
    pre_state(c1,1)     = dynamics_model.current_state();
    pos_state(c1,1)     = dynamics_model.sample(stim_params(c1,:));
end

x0              = pre_state;
x1              = pos_state;
u1              = stim_params;

% Learning
for c1 = n_burn_in+1:n_burn_in + n_learning

    % Create new policy estimate with current data
    policy_est      = simulation_gp();
    policy_est.initialize_data(x0, u1, x1, lower_bound, upper_bound)

    % Get current state from dynamics model
    current_state   = dynamics_model.current_state;

    % Calculate optimal input based on current state
    optimal_est(c1) = policy_est;

    % Get new stimulation parameters
    u1(c1,:)        = sdbo_opt_update(x0, u1, x1, params, lower_bound, upper_bound);

    % Sample new stimulation parameters and get new state
    x1(c1,:)        = dynamics_model.sample(u1(c1,:));
    x0(c1,:)        = current_state;
       
end

end

