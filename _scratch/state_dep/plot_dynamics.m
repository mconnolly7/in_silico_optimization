function  plot_dynamics()


delta       = 1:4;
theta       = 4:10;
alpha       = 8:15;
beta        = 16:31;
gamma       = 32:50;

% freq_bands  = {delta, theta, alpha, beta, gamma};
freq_bands  = {alpha};

for c1 = 1:size(freq_bands,2)
    figure
    f_band          = freq_bands{c1};
    
    [X0, X1, U1]    = build_dataset([gamma theta]);

    Y   = X1;
    joint_model = gp_object();
    joint_model.initialize_data([X0, U1], 2e-9-Y);
    joint_model.minimize(5);

    static_model = gp_object();
    static_model.initialize_data(U1, 2e-9-Y);
    static_model.minimize(5);

    state_model = gp_object();
    state_model.initialize_data(X0, 2e-9-Y);
    state_model.minimize(5);

    subplot(1,3,1);
    joint_model.plot_mean
    % state_model.plot_standard_deviation

    subplot(1,3,2);
    static_model.plot_mean
    static_model.plot_standard_deviation

    subplot(1,3,3);
    state_model.plot_mean
    state_model.plot_standard_deviation
    hold on
    plot([0 0], 5e-9*[1 1])
end
end

