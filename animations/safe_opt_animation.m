clear 
params  = [1 3 3];

safe_samples    = [0 .5 1]';
lower_bound     = 0;
upper_bound     = 7;
input_space     = (lower_bound:.05:upper_bound)';

% [ds_lower safety_lower; ds_upper safety_upper];
y_bounds        = [-1 -1; 1 1];
n_samples       = 30;
n_trials        = 1;

USE_HYPERPRIOR  = 1;
USE_SAFE_OPT    = 1;
PLOT            = 1;

v = VideoWriter('safe_opt_S1_H1.jpeg', 'Motion JPEG AVI' );

threshold       = 0;

load('memory_project/data/modeled_data/ds_models_2020_02_02.mat');
subject_idx = 6;
 
fprintf('Subject: %d\n', subject_idx)

BETA            = params(1);
ETA             = params(2);
ALPHA           = params(3);

tic
ds_model = ds_models(subject_idx);

close all
f = figure('Position', [731 242 796 557])
set(gcf,'color','w');

rng(2)
[X_opt, Y_opt, X_sample, Y_sample, Y_safety, X_frames] = safe_opt_loop_memory(ds_model, ds_model,...
    threshold, BETA, ETA, ALPHA, PLOT, input_space, y_bounds, safe_samples, n_samples, USE_HYPERPRIOR, USE_SAFE_OPT, n_trials);
toc 

X_frames = reshape(X_frames,[], 1);

%%
v.FrameRate = 30;


X_mat = repmat(X_frames, 1, 6);
X_frames = reshape(X_mat',[],1);
open(v)
for c1 = 1:size(X_frames,1)
   writeVideo(v, X_frames{c1});
end
close(v)
    
    
    


