
rng(0);


[x_gt, y_gt, gp_model] = objective_data();
lower_bound = gp_model.lower_bound;
upper_bound = gp_model.upper_bound;

theta_1     = 0;
theta_2     = -.5;
perturb_x   = [-.4 .4];

%% Panel 1
set(f, 'currentaxes', ax(1));
gp_model.plot_mean
gp_model.plot_standard_deviation

perturb_y   = gp_model.sample(perturb_x');
xx          = [perturb_x; perturb_x];
yy          = [-1 -1; perturb_y' ];

plot(xx,yy, 'color', 'k');

scatter(theta_1, -1, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[27,158,119]/255,...
    'LineWidth',2)

scatter(perturb_x, [-1 -1], 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.5 .5 .5],...
    'LineWidth',2)

scatter(perturb_x, perturb_y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.5 .5 .5],...
    'LineWidth',2)


dim = [.16 .71 .3 .3];
str = sprintf('\\theta_1');
annotation(f, 'textbox', dim, 'String', str, 'FitBoxToText', 'on', ...
    'verticalalignment', 'bottom', 'FontSize', 25, 'Edgecolor', 'none');

ylim([-1.5,2])
set(gca,'FontSize', 16)
title('SPSA', 'FontSize', 24)

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off

%% Panel 2

set(f, 'currentaxes', ax(4));
gp_model.plot_mean
gp_model.plot_standard_deviation

scatter(perturb_x, perturb_y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.5 .5 .5],...
    'LineWidth',2)

scatter(theta_1, -1, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[27,158,119]/255,...
    'LineWidth',2)

dim = [.16 .4 .3 .3];
str = sprintf('\\theta_1');
annotation(f, 'textbox', dim, 'String', str, 'FitBoxToText', 'on', ...
    'verticalalignment', 'bottom', 'FontSize', 25, 'Edgecolor', 'none');

dim = [.17 .57 .3 .3];
str = sprintf('Approx. Gradient');
annotation(f, 'textbox', dim, 'String', str, 'FitBoxToText', 'on', ...
    'verticalalignment', 'bottom', 'FontSize', 20, 'Edgecolor', [27,158,119]/255, 'LineWidth', 3);

xa = [.21 .12];
ya = [.51 .62];
annotation('arrow',xa,ya, 'LineWidth', 2, 'color', [27,158,119]/255)

ylim([-1.5,2])
xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off

%% Panel 3

set(f, 'currentaxes', ax(7));
gp_model.plot_mean
gp_model.plot_standard_deviation

scatter(theta_1, -1, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[27,158,119]/255,...
    'LineWidth',2)

dim = [.16 .09 .3 .3];
str = sprintf('\\theta_1');
annotation(f, 'textbox', dim, 'String', str, 'FitBoxToText', 'on', ...
    'verticalalignment', 'bottom', 'FontSize', 25, 'Edgecolor', 'none');

scatter(theta_2, -1, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[27,158,119]/255,...
    'LineWidth',2)

dim = [.14 .09 .3 .3];
str = sprintf('\\theta_2');
annotation(f, 'textbox', dim, 'String', str, 'FitBoxToText', 'on', ...
    'verticalalignment', 'bottom', 'FontSize', 25, 'Edgecolor', 'none');

ylim([-1.5,2])
ylabel('Output (e.g. Gamma Power)');
xlabel('Input (e.g. Frequency)')

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off
set(gca,'FontSize', 18)
