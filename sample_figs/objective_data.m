function [x, y_meas, gp_model] = objective_data()
rng(0)

x           = -3:.01:3;
y1          = 2*normpdf(x, -1, .6);
y2          = normpdf(x, 1, .6);

y           = y1+y2;
y_meas      = y + randn(size(y))/2.5;

gp_model    = gp_object();
gp_model.initialize_data(x', y_meas', min(x), max(x));
gp_model.covariance_function = {@covSEard};
gp_model.minimize(10)

end

