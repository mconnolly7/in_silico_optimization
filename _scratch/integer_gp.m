close all

modeled_data_dir    = 'modeled_data/evoked_potential/';
model_path          = [modeled_data_dir 'EP002'];
load(model_path);

t               = gp_model.t;
rand_idx        = randperm(size(t,1));

t_rand          = t(rand_idx(1:300),:);
t_round         = t_rand;
t_round(:,1)    = round(t_rand(:,1));

y               = gp_model.predict(t_round);
y_rand          = y;

gp_int          = gp_object();

% t_round(:,1) = round(t_round(:,1));

gp_int.initialize_data(t_rand, y_rand, min(t), max(t));
gp_int.covariance_function  = {@covSEard_int};

% gp_int.covariance_function  = {@covMaternard_int, 3};
gp_int.minimize(1);

gp_int.plot_mean
gp_int.plot_confidence_interval
