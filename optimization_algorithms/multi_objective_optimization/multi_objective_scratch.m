function [gt_cost, gtp, pareto_history ] =  multi_objective_scratch
close all
burn_in         = 10;
n_samples       = 100;
PLOT            = 1;
% Load the neuro model
modeled_data_dir    = 'evoked_potential_project/data/modeled_data/';
model_path          = [modeled_data_dir 'EP002'];

load(model_path);

lower           = gp_model.lower_bound;
upper           = gp_model.upper_bound;

%
% Get ground truth Pareto front from neuromodule
%
n_gt            = 1000;
gtx1            = round(uniform_range(lower(1)-.5, upper(1)+.5,n_gt));
gtx2            = uniform_range(lower(2), upper(2),n_gt);

gt2             = gp_model.predict([gtx1 gtx2]);
gt1             = 1 - gtx2/max(gtx2);
gt_cost         = [gt1 gt2];
gtp             = get_pareto(gt_cost);

%
% Generate 5 samples from uniform dist for burn-in
%
X_amplitude     = uniform_range(lower(2), upper(2),burn_in);
X_cathode       = round(uniform_range(lower(1)-.5, upper(1)+.5,burn_in));
X               = [X_cathode X_amplitude];

%
% Evaluate samples
%
ep_amplitude    = gp_model.sample(X);

for c1 = burn_in+1:n_samples

    %
    % Calculate objectives
    %
    ep_power            = X(:,2);
    ep_efficiency       = 1 - ep_power/max(ep_power);
    
    %
    % Adding jitter to data otherwise GP doesn't behave. Could be replace 
    % with a linear/less sophisticated model, but this could probably 
    % generalize to other situations better
    %
    ep_efficiency       = ep_efficiency + randn(size(ep_efficiency,1),1)*.01;
    
    
    %
    % Construct GP model of objectives
    %
    objective_1       	= gpr_model();
    objective_2      	= gpr_model();
     
    objective_1.initialize_data(X, ep_efficiency, lower, upper)
    objective_2.initialize_data(X, ep_amplitude, lower, upper)
    
    objective_2.covariance_function    = {@covSEard};
    objective_2.hyperparameters.cov    = [0; 0; log(std(ep_amplitude)/sqrt(2))];
    
    prior.cov = { {}; {}; {@priorDelta}};
    inf = {@infPrior,@infExact,prior};
    objective_2.inference_method = inf;

   
    %
    % Fit models
    %
    objective_1.minimize(100)
    objective_2.minimize(100);
    
   
    %
    % Generate random set of candidate points
    %
    X1_cand             = uniform_range(lower(1)-.5, upper(1)+.5,1000);
    X1_cand             = round(X1_cand);   
    X2_cand             = uniform_range(lower(2), upper(2),1000);
    X_cand              = [X1_cand X2_cand];
    
    %
    % Evaluate objective on set of candidate points
    %
    [f1,~,~,s1] = objective_1.predict(X_cand);
    [f2,~,~,s2] = objective_2.predict(X_cand);
        
    
    %
    % Calculate estimated Pareto front
    %
    costs_est   = [f1 f2];
    pareto_idx  = get_pareto(costs_est);   
    pareto_est  = costs_est(pareto_idx == 1,:);
    
    % Normalize the value and uncertainty of each objective
    
    
    %
    % Calculate EHVI for each candidate point
    %
    fprintf('Trial: %d\n ', c1)
    X_sub = floor(linspace(1,size(X_cand,1),100));
    for c2 = 1:size(X_cand(X_sub,:),1) 
        idx         = X_sub(c2);
        f_cand      = [f1(idx,:),f2(idx,:)];
        s_cand      = [s1(idx,:),s2(idx,:)];
        ref_point   = [0, 0];
        
        pareto_sub  = floor(linspace(1,size(pareto_est,1),25));
        ehvi(c2)    = exi2d(-1*pareto_est(pareto_sub,:),ref_point,-1*f_cand,10*s_cand); 
        fprintf('|')
    end   
    fprintf('\n');
    
    
    %
    % Select point with best EHVI
    %
    [~, max_ehvi_idx]   = max(ehvi);
    X_cand_idx          = X_sub(max_ehvi_idx);
    X_new               = X_cand(X_cand_idx,:);

    
    %
    % Evaluate new sample
    %
    X                   = [X; X_new];
    ep_amplitude(c1)    = gp_model.sample(X_new);
      
    
    %%%%%%%%%%%%%%
    % PLOT CODE
    %%%%%%%%%%%%%%
    if PLOT
        subplot(1,3,1)
        objective_1.plot_mean()
        %objective_1.plot_confidence_interval
        xlabel('Cathode')
        ylabel('Stimulation Amplitude (mA)')
        zlabel('Efficiency')
        set(gca, 'FontSize', 18)
        hold off

        %
        %
        %
        subplot(1,3,2)
        objective_2.plot_mean()
        %objective_2.plot_confidence_interval
        xlabel('Cathode')
        ylabel('Stimulation Amplitude (mA)')
        zlabel('EP Magnitude')
        set(gca, 'FontSize', 18)
        hold off

        %
        %
        %
        subplot(1,3,3)
        p1                      = objective_1.predict([gtx1 gtx2]);
        p2                      = objective_2.predict([gtx1 gtx2]);

        pareto_idx              = get_pareto([p1 p2]);
        pareto_est              = [p1(pareto_idx == 1), p2(pareto_idx == 1)];

        [p1_sort, p1_sort_idx]  = sort(pareto_est(:,1));
        p2_sort                 = pareto_est(p1_sort_idx,2);

        scatter(gt_cost(gtp == 0,1),gt_cost(gtp == 0,2),100, 'MarkerEdgeColor', .5*ones(1,3))
        hold on
        scatter(gt_cost(gtp == 1,1),gt_cost(gtp == 1,2),100, 'MarkerEdgeColor', [.5 0 0])

        pareto_history{c1} = [p1_sort, p2_sort];

%         for c2 = burn_in+1:size(pareto_history,2)-1
%             plot(pareto_history{c2}(:,1), pareto_history{c2}(:,2), ...
%                 'color', [.3 .3 .3], 'LineWidth', 3);
%         end
        plot(pareto_history{end}(:,1), pareto_history{end}(:,2), 'color', 'k', 'LineWidth', 3);

        xlabel('Efficiency (a.u.)')
        ylabel('EP Magnitude (mV)')
        set(gca, 'FontSize', 18)
        hold off

        drawnow
    end
    %%%%%%%%%%%%%%
    % END PLOT
    %%%%%%%%%%%%%%
end
end

function pareto_idx = get_pareto(costs)
pareto_idx  = ones(size(costs,1),1);

for c2 = 1:size(costs,1)
    c               = costs(c2,:);
    pareto_idx(c2)  = all(any(c >= costs,2));
end

end





