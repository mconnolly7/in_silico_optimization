function pareto_idx = get_pareto(costs)
pareto_idx  = ones(size(costs,1),1);

for c2 = 1:size(costs,1)
    c               = costs(c2,:);
    pareto_idx(c2)  = all(any(c >= costs,2));
end

end