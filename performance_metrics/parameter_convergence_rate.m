function b = parameter_convergence_rate(optimal_est, gp_model)

lower_bound     = gp_model.lower_bound;
upper_bound     = gp_model.upper_bound;

sample_number   = optimal_est(2:end,1);
optimal_est     = optimal_est(:,2:end);

% Normalize parameters 
optimal_est_norm    = linear_normalize_X(optimal_est, lower_bound, upper_bound);
position            = sqrt(sum(optimal_est_norm.^2,2));
d_position          = abs(diff(position));

f = fit(sample_number,d_position,'exp1', 'Startpoint',[0 0]);
b = f.b;

end

