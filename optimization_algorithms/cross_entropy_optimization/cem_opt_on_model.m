function [pos_state, stim_params, optimal_est] = cem_opt_on_model(objective_model, n_samples, params)

% close all

lower_bound         = repmat(objective_model.lower_bound, 1);
upper_bound         = repmat(objective_model.upper_bound, 1);
   
n_iteration         = params(1);
samples_per_iter    = n_samples / n_iteration;
n_elite             = .2 * samples_per_iter;
learning_rate       = params(2);

m                   = (upper_bound - lower_bound)/2;
b                   = (upper_bound + lower_bound)/2;

sigma               = eye(length(lower_bound));
mu                  = zeros(size(lower_bound));
norm_cov            = norm(sigma);

x_values_norm       = [];
x_values            = [];
pos_state           = [];
iterations          = [];

for c1  = 1:n_iteration
    alpha = learning_rate;
        
    for c2 = 1:samples_per_iter
        
        new_sample          = get_new_sample(-1, 1, mu(c1,:),sigma(:,:,c1));
        scaled_sample       = m.*new_sample + b;
        
        new_state           = objective_model.sample(scaled_sample);
        
        x_values_norm       = [x_values_norm; new_sample];
        x_values            = [x_values; scaled_sample];
        pos_state           = [pos_state; new_state];
        iterations          = [iterations; c1];
        
    end
    
    iter_start_idx      = (c1-1)*samples_per_iter+1;
    y_mean              = pos_state(iter_start_idx:end);
    
    [q, sort_idx]       = sort(y_mean, 'descend');
    elite_idx           = sort_idx(1:n_elite) + iter_start_idx - 1;
    elite               = squeeze(x_values_norm(elite_idx,:));
    
    mu(c1+1,:)          = alpha*mean(elite) + (1-alpha)*mu(c1,:);
    sigma(:,:,c1+1)     = alpha*cov(elite,1)  + (1-alpha)*sigma(:,:,c1);
    norm_cov(c1+1)      = norm(sigma(:,:,c1+1));    
    
    mu_scaled           = mu(c1+1,:) .* m + b;
    optimal_est(c1,:)   = [c1*samples_per_iter mu_scaled];
end
 
stim_params = x_values;
end


%%%%%
%
%%%%%
function new_sample = get_new_sample(lower_bound, upper_bound, mu, sigma)

new_sample  = mvnrnd(mu,sigma,1);

while any(new_sample < lower_bound | new_sample > upper_bound)
    new_sample  = mvnrnd(mu,sigma,1);
end

end

