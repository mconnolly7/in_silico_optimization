function cost = SPSA_cost_function(P)

load('modeled_data/R1_gamma.mat')

[spsa_pos_state, spsa_stim_params, spsa_optimal_est]    ...
            = spsa_opt(gp_model, 1, 200, P);

for c1 = 1:30
    best_param = gp_model.discrete_extrema(2);
    cost(c1)     = error_from_optimum(spsa_optimal_est, best_param, gp_model);
end

cost = mean(cost);

if ~isreal(cost)
   cost = 1;
end
end

