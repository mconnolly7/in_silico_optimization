function [X_opt, Y_opt, X_sample, Y_sample, Y_safety] = safe_opt_on_model(objective_model, safety_model, objective_init, safety_init,...
    threshold, beta, eta, alpha, PLOT, input_space, y_bounds, safe_seed, n_samples, USE_HYPERPRIOR, USE_SAFE)

n_safe          = size(safe_seed,1);
X_sample        = safe_seed;
Y_sample        = min(y_bounds(2,1), objective_model.sample(safe_seed));
Y_safety        = max(y_bounds(1,2), safety_model.sample(safe_seed));

if all(Y_sample == 1)
   Y_sample = [1 .99 1]';
end

for c1 = n_safe+1:n_samples
        
    [x_sample, x_opt, y_opt, S, ucb] ...
        = safe_opt_update(X_sample, Y_sample, Y_safety, objective_init, safety_init, beta, eta, alpha, safe_seed, threshold, ...
        input_space, USE_HYPERPRIOR, USE_SAFE, PLOT);
    
    y_sample                = objective_model.sample(x_sample);  
    y_sample                = min(y_sample, y_bounds(2,1));
    y_sample                = max(y_sample, y_bounds(1,1));

    y_safety                = safety_model.sample(x_sample);  
    y_safety                = min(y_safety, y_bounds(2,2));
    y_safety                = max(y_safety, y_bounds(1,2));
    y_safety                = round(y_safety);
    
    X_sample(c1,:)          = x_sample;
    Y_sample(c1,1)          = y_sample;
    Y_safety(c1,1)          = y_safety;
    
    X_opt(c1,:)             = x_opt;
    Y_opt(c1,:)             = y_opt;
    
    if 1
        
        subplot(2,3,3); hold off
        plot(1:c1, X_sample(:,1), '--')
        hold on
        plot(1:c1, X_opt(:,1), '-', 'LineWidth', 2)
        xlim([1 n_samples])
        
        subplot(2,3,6); hold off
        plot(1:c1, X_sample(:,2), '--')
        hold on
        plot(1:c1, X_opt(:,2), '-', 'LineWidth', 2)
        xlim([1 n_samples])

        drawnow            
    end

end
end


