function new_sample = constrained_normrnd(lower_bound, upper_bound, mu, sigma, n_samples)

if nargin < 5
    n_samples = 1;
end

for c1 = 1:n_samples
    new_sample(c1,:)  = mvnrnd(mu,sigma,1);

    while any(new_sample(c1,:) < lower_bound | new_sample(c1,:) > upper_bound)
        new_sample(c1,:)  = mvnrnd(mu,sigma,1);
    end
end