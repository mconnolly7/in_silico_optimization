function dr_merge_n_scratch

load('simulation_results/optogenetic/3D/R1_gamma_3D_simulation_results_parameter_sweep.mat');
new_data = load('simulation_results/optogenetic/R1_gamma_3D_simulation_results_parameter_sweep_test_1.mat');


e_converge(end,:)  = new_data.e_converge(end,:);
e_dist(end,:)      = new_data.e_dist(end,:);
e_sample(end,:)    = new_data.e_sample(end,:);
e_trace(end,:)     = new_data.e_trace(end,:);

opt_params(end,:)  = new_data.opt_params(end,:);
optimal_est(end,:) = new_data.optimal_est(end,:);
p_converge(end,:)  = new_data.p_converge(end,:);
p_dist(end,:)      = new_data.p_dist(end,:);
stim_params(end,:) = new_data.stim_params(end,:);

save('simulation_results/optogenetic/3D_merge/R1_merge' , 'p_converge', 'p_dist', 'e_converge', 'e_dist',...
        'opt_params', 'model_metadata', 'optimizer', 'e_sample', 'e_trace',...
        'optimal_est', 'stim_params', 'optimal_ground_truth')
end