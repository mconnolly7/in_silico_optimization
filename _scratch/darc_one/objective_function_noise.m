function objective_val = objective_function_noise(next_param, objective_means)

objective_val  = objective_function(next_param, objective_means) + randn/10;

end