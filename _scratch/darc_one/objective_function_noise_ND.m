function objective_val = objective_function_noise_ND(next_param, objective_means, objective_noise, scale)

objective_val  = scale * (objective_function_ND(next_param, objective_means , 1) + randn * objective_noise);

end