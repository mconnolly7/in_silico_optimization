% A minimalistic sparse Echo State Networks demo with Mackey-Glass (delay 17) data 
% in "plain" Matlab/Octave.
% from https://mantas.info/code/simple_esn/
% (c) 2012-2020 Mantas Lukosevicius
% Distributed under MIT license https://opensource.org/licenses/MIT
function minimalESN_original 
% load the data
train_len   = 2000;
test_len    = 2000;
init_len    = 100;
data    = load('MackeyGlass_t17.txt');

% plot some of it
% figure(10);
% plot(data(1:1000));
% title('A sample of data');

% generate the ESN reservoir
in_size     = 1; 
out_size    = 1;
res_size    = 100;
leak_rate 	= 0.3; % leaking rate

rand( 'seed', 42 );

W_in        = (rand(res_size,1+in_size)-0.5) .* 1;
W           = rand(res_size,res_size)-0.5;

% normalizing and setting spectral radius
disp 'Computing spectral radius...';
opt.disp    = 0;
rhoW        = abs(eigs(W,1,'LM',opt));
W           = W .* (1.25 / rhoW);
disp 'done.'

% allocated memory for the design (collected states) matrix
X           = zeros(1+in_size+res_size,train_len-init_len);

% set the corresponding target matrix directly
Yt          = data(init_len+2:train_len+1)';

% run the reservoir with the data and collect X
x = zeros(res_size,1);
for t = 1:train_len
	u = data(t);
	x = (1-leak_rate)*x + leak_rate*tanh( W_in*[1;u] + W*x );
	if t > init_len
		X(:,t-init_len) = [1;u;x];
	end
end

% train the output by ridge regression
reg = 1e-8;  % regularization coefficient
% direct equations from texts:
%X_T = X'; 
%Wout = Yt*X_T * inv(X*X_T + reg*eye(1+inSize+resSize));
% using Matlab mldivide solver:
W_out = ((X*X' + reg*eye(1+in_size+res_size)) \ (X*Yt'))'; 


% W_out = rand(1,1+in_size+res_size);

rc_struct.out_size      = out_size;
rc_struct.test_len      = test_len;
rc_struct.train_len     = train_len;
rc_struct.data          = data;

rc_struct.W_in          = W_in;
rc_struct.W             = W;
rc_struct.W_out         = W_out;
rc_struct.leak_rate  	= leak_rate;
rc_struct.x             = x;

lower_bound             = -100;
upper_bound             = 100;

eval_model(W_out, rc_struct, 1)

options                 = optimoptions('ga','ConstraintTolerance',1e-6,'PlotFcn', @gaplotbestf, 'PopulationSize', 1000);

PLOT                    = 0;
n_vars                  = 1 + in_size + res_size;
fun                     = @(W_test)eval_model(W_test, rc_struct, PLOT);

W_out_ga                = ga(fun,n_vars,[],[],[],[],lower_bound,upper_bound,[],options);

PLOT                    = 1;
figure
eval_model(W_out_ga, rc_struct, PLOT)

figure
plot(W_out)
hold on
plot(W_out_ga);
% % plot some signals
% figure(1);

% hold off;
% axis tight;
% title('Target and generated signals y(n) starting at n=0');
% legend('Target signal', 'Free-running predicted signal');
% 
% figure(2);
% plot( X(1:20,1:200)' );
% title('Some reservoir activations x(n)');
% 
% figure(3);
% bar( Wout' )
% title('Output weights W^{out}');

end




function error = eval_model(W_test, rc_struct, PLOT)

out_size    = rc_struct.out_size;
test_len    = rc_struct.test_len;
train_len   = rc_struct.train_len;
data        = rc_struct.data;

W_in        = rc_struct.W_in;
W           = rc_struct.W;
W_out       = W_test;
leak_rate  	= rc_struct.leak_rate;

x           = rc_struct.x;
Y           = zeros(out_size, test_len);
u           = data(train_len+1);

for t = 1:test_len 
    
	x       = (1-leak_rate) * x + leak_rate * tanh(W_in * [1; u] + W * x);
	y       = W_out * [1;u;x];
	Y(:,t)  = y;

    u       = y;
end

% compute MSE for the first errorLen time steps
errorLen    = 500;
mse         = sum((data(train_len+2:train_len+errorLen+1)'-Y(1,1:errorLen)).^2)./errorLen;
l2_norm     = norm(W_out);

error       = mse + 1e-8*l2_norm;
% disp(['MSE = ', num2str(mse)]);

if PLOT
    plot( data(train_len+2:train_len+test_len+1), 'color', [0,0.75,0] );
    hold on;
    plot( Y', 'b' );
end

end


