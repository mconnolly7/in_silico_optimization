function [outlier_idx, in_dist] = detect_outlier(X, threshold)

if nargin < 2
   threshold    = 3.5; 
end

outlier_idx = zeros(size(X,1),1);
for c1 = 1:size(X,2)
    o_score     = abs(X(:,c1) - median(X(:,c1))) / (1.48 * mad(X(:,c1),1));
    outlier_idx = outlier_idx | (o_score > threshold) | all(X(:,c1) == 0);
end

in_dist = X(~outlier_idx,:);
end

