function get_expanders()
% Check for expanders
if USE_EXPANDERS 
    for c2 = 1:size(inputs)

        v_test          = [u_v; inputs(c2)];
        area_test       = [y_area; Q_high(c2)];

        surrogate_test  = get_gp(v_test,area_test, hyp);

        [mean_test, ~, ~, var_test] = surrogate_test.predict(inputs);
        low_test        = mean_test - beta * var_test;
        low_idx         = low_test >= 0;
        v_expanded      = S == 0 & low_idx == 1;

        if any(v_expanded)
            G_safe(c2) = 1;
        end

    end   
end
end