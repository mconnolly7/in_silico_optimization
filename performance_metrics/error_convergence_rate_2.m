function [e_mean, e_ci] = error_convergence_rate_2(sample_number, est_optimal_y)

% sample_number(end+1)    = sample_number(end)+1;
% est_optimal_y           = [.5*ones(1, size(est_optimal_y,2)) ; est_optimal_y];

n_trials    = size(est_optimal_y,2);
sample_vec  = repmat(sample_number, n_trials, 1);

error_vec   = reshape(est_optimal_y,[],1);

f        	= fit(sample_vec, error_vec, 'exp1', 'Startpoint',[0 0]);

e_mean      = f.b;
f_ci        = confint(f);
e_ci        = f_ci(:,2);
end

