close all;
fs          = 10000;
dur         = 1; 
t           = (1:dur * fs)/fs;

hz = [7, 211, 149, 43, 18, 135, 130, 96];
for c1 = 1:8
    subplot(4,2,c1)
    
    train_freq  = 1/hz(c1);
    t_train     = hz(c1)/fs;

    r           = mod(t,train_freq);

    train_carry  = zeros(size(t));
    f_peaks         = findpeaks(r);
    train_carry(f_peaks.loc) = 1;
    plot(t, train_carry)
    title(sprintf('%dHz', hz(c1)));
    yticks([]);
    if c1 ~= 7
        xticklabels([]);
    else
        xlabel('Seconds')
    end
    ylim([-.1 1.1])
end


%%
pulse_train = (pulse_carry & train_carry) + normrnd(0,.2,size(pulse_train)); 

hold on
% plot(t, train_freq * 1)
plot(t, pulse_freq * 2)
% plot(t, pulse_train * 3)

params.Fs       = fs;
params.tapers   = [3 5];
[S, F]          = mtspectrumc(pulse_train, params);

theta           = [4 10];
subplot(1,2,1)
plot(t, pulse_train)

subplot(1,2,2)
plot(F, S);





