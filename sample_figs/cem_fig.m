
rng(0);


[x_gt, y_gt, gp_model] = objective_data();
lower_bound = gp_model.lower_bound;
upper_bound = gp_model.upper_bound;

mu          = 0;
sigma       = 1.5;
n_samples   = 20;
n_elite     = 5;

%% Panel 1
set(f, 'currentaxes', ax(2));
gp_model.plot_mean
gp_model.plot_standard_deviation

sample_dist = normpdf(x_gt,mu,sigma);
x_cand      = constrained_normrnd(lower_bound,upper_bound, mu, sigma, n_samples);

xx          = [x_gt flip(x_gt)];
dd          = [sample_dist-1 -2*ones(size(sample_dist))];
patch(xx, dd, [217,95,2]/255, 'FaceAlpha', .4)

plot(x_gt, sample_dist-1, 'LineWidth', 2, 'color', [217,95,2]/255)
scatter(x_cand, -1*ones(size(x_cand)), 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.5 .5 .5],...
    'LineWidth',2)

ylim([-1.5,2])

dim = [.5 .48 .3 .3];
str = sprintf('\\mu_1 = %.1f, \\sigma_1 = %.1f', mu, sigma);
annotation('textbox',dim,'String',str,'FitBoxToText','on',...
    'FontSize', 25, 'Edgecolor', [217,95,2]/255, 'LineWidth', 3, 'BackgroundColor', 'w');
set(gca,'FontSize', 16)
title('CEM', 'FontSize', 24)

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off

%% Panel 2
set(f, 'currentaxes', ax(5));
gp_model.plot_mean
gp_model.plot_standard_deviation

y_meas = gp_model.sample(x_cand);

[y_sort, y_sort_idx] = sort(y_meas,'descend');

xx          = [x_gt flip(x_gt)];
dd          = [sample_dist-1 -2*ones(size(sample_dist))];
patch(xx, dd, [217,95,2]/255, 'FaceAlpha', .4)
plot(x_gt, sample_dist-1, 'LineWidth', 2, 'color', [217,95,2]/255)

xx          = [x_cand x_cand]';
yy          = [-1*ones(size(x_cand)) y_meas]';
elite_idx   = y_sort_idx(1:n_elite);
reg_idx     = y_sort_idx(n_elite+1:end);



plot(xx(:,elite_idx), yy(:,elite_idx),'color', [217,95,2]/255); 

plot(xx(:,reg_idx), yy(:,reg_idx), 'k'); 

scatter(x_cand(reg_idx), -1*ones(size(x_cand(reg_idx))), 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.5 .5 .5],...
    'LineWidth',2)
scatter(x_cand(elite_idx), -1*ones(size(x_cand(elite_idx))), 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[217,95,2]/255,...
    'LineWidth',2)

scatter(x_cand(elite_idx), y_meas(elite_idx), 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[217,95,2]/255,...
    'LineWidth',2);
scatter(x_cand(reg_idx), y_meas(reg_idx), 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.5 .5 .5],...
    'LineWidth',2);

ylim([-1.5,2])

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off

%% Panel 3

set(f, 'currentaxes', ax(8));
gp_model.plot_mean
gp_model.plot_standard_deviation

mu_2        = mean(x_cand(elite_idx));
sigma_2     = std(x_cand(elite_idx));

sample_dist = normpdf(x_gt,mu_2,sigma_2);
x_cand      = constrained_normrnd(lower_bound,upper_bound, mu_2, sigma_2, n_samples);

xx          = [x_gt flip(x_gt)];
dd          = [sample_dist-1 -2*ones(size(sample_dist))];
patch(xx, dd, [217,95,2]/255, 'FaceAlpha', .4)

plot(x_gt, sample_dist-1, 'LineWidth', 2, 'color', [217,95,2]/255)
scatter(x_cand, -1*ones(size(x_cand)), 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.5 .5 .5],...
    'LineWidth',2)

ylim([-1.5,2])

dim = [.5 0.1 .3 .3];
str = sprintf('\\mu_2 = %.1f, \\sigma_2 = %.1f', mu_2, sigma_2);
annotation(f,'textbox',dim,'String',str,'FitBoxToText','on', ...
    'verticalalignment', 'bottom', 'FontSize', 25, 'EdgeColor', [217,95,2]/255,...
    'LineWidth', 3, 'BackgroundColor', 'w');

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off