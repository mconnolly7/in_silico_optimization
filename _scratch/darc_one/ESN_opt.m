n_trials            = 5;
spectral_radius     = 0.2;
reservior_size      = 15;

clc
rng(1,'twister')
n_vars              = 5;
n_trials        	= 5; floor(uniform_range(1, 20, 20));
spectral_radius  	= .1:.2:1; uniform_range(.01, 1, 20);
reservior_size      = 20; floor(uniform_range(1, 25, 20));

for c1 = 1:5
    
    [W_out_ga{c1}, f_val{c1}, exit_flag{c1}, output{c1}, population{c1}, scores{c1}, rc_struct(c1)]  ...
        = train_ESN_opt(n_vars,1, n_trials, spectral_radius(c1), reservior_size); 
        
    for c2 = 1:50
        tic
        error_ESN_1(c1,c2)       = eval_model_ND(rc_struct(c1).W_out_ga, rc_struct(c1), 1, 0);
        ESN_opt_time_1(c1,c2)    = toc;
    end
end


%%
clear error_ESN
tic
for c1 = 1:50
    tic
    error_ESN(c1)       = eval_model_2D(rc_struct(1).W_out_ga, rc_struct(1), 1, 0);
    ESN_opt_time(c1)    = toc;
end

%%
error = eval_model_2D(W_out_ga, rc_struct, 1, 1);

%%
% clear
close all
clc
n_trials        = 10;
n_samples       = 100;
n_D             = 5;
n_burn_in       = 30;
n_burn_in       = 100;
x1              = -4:.5:4;
input_space     = x1;

for c1 = 2:n_D
    input_space     = combvec(input_space,x1);
end

input_space     = input_space';

tic
for c1 = 1:n_trials
    c1
    tic
    bao_opt                 = gpr_model();
    bao_opt.acquisition_function = 'UCB';
    
    
    objective_means         = rand(1,n_D)*8-4;
%     [objective_max, idx]    = max(objective_function_ND(input_space, objective_means, 1));
%     optimal_param           = input_space(idx,:);
    
    objective_max           = objective_function_ND(objective_means, objective_means, 1);
    optimal_param           = objective_means(1,:);
    
    objective_scale         = 1/objective_max;
    objective_noise         = objective_max * 0.01;
          
    for c2 = 1:n_samples
        if c2 >= n_burn_in && mod(c2,2) == 0
            bao_opt.initialize_data(input_param,  objective_val, -4*ones(1,n_D), 4*ones(1,n_D));
%             bao_opt.minimize(1)
            input_param(c2,:) = bao_opt.ga_acquisition_function(2.5);
        else           
         	input_param(c2,:)   = rand(1,n_D) * 8-4;
        end
            
        objective_val(c2,1)   	= objective_function_noise_ND(input_param(c2,:), objective_means, objective_noise, objective_scale);
        
        % Get the max parameter
        if c2 > n_burn_in
            max_param(c2,:)    	= bao_opt.ga_find_max();
        else
            [max_val_noise(c2), max_val_idx]    = max(objective_val);
            max_param(c2,:)    	= input_param(max_val_idx,:);
        end
        
        subplot(2,1,1)
        if n_burn_in == n_samples
            ESN_max_val(c1,c2)         	= objective_function_ND(max_param(c2,:), objective_means(1,:), objective_scale);
            plot(ESN_max_val(c1,:))
%             plot(max_param(:,1))

        else
            BAO_max_val(c1,c2)         	= objective_function_ND(max_param(c2,:), objective_means(1,:), objective_scale);
            plot(BAO_max_val(c1,:))
%             plot(max_param(:,c1))
            
        end
        ylim([0 1]);
        xlim([1 n_samples]);
        p = parula(5);
        subplot(2,1,2)

        cla
        hold on
        x1 = combvec(-4:.01:4, objective_means(1,2), objective_means(1,3),objective_means(1,4),objective_means(1,5))';
        plot(-4:.01:4, objective_function_ND(x1, objective_means, objective_scale), 'color', p(1,:));
        x0 = objective_means; x0(1) = max_param(c2,1);
        plot(max_param(c2,1), objective_function_ND(x0, objective_means, objective_scale), 'o', 'color', p(1,:), 'MarkerEdgeColor', 'k', 'MarkerSize', 10, 'MarkerFaceColor', p(1,:))
             
        x2 = combvec(objective_means(1,1), -4:.01:4,objective_means(1,3), objective_means(1,4),objective_means(1,5))';
        plot(-4:.01:4, objective_function_ND(x2, objective_means, objective_scale), 'color', p(2,:));
        x0 = objective_means; x0(2) = max_param(c2,2);
        plot(max_param(c2,2), objective_function_ND(x0, objective_means, objective_scale), 'o', 'color', p(2,:), 'MarkerEdgeColor', 'k', 'MarkerSize', 10, 'MarkerFaceColor', p(2,:))
        
        x3 = combvec(objective_means(1,1), objective_means(1,2),-4:.01:4, objective_means(1,4),objective_means(1,5))';
        plot(-4:.01:4, objective_function_ND(x3, objective_means, objective_scale), 'color', p(3,:));
        x0 = objective_means; x0(3) = max_param(c2,3);
        plot(max_param(c2,3), objective_function_ND(x0, objective_means, objective_scale), 'o', 'color', p(3,:), 'MarkerEdgeColor', 'k', 'MarkerSize', 10, 'MarkerFaceColor', p(3,:))
        
        x4 = combvec(objective_means(1,1), objective_means(1,2),objective_means(1,3), -4:.01:4,objective_means(1,5))';
        plot(-4:.01:4, objective_function_ND(x4, objective_means, objective_scale), 'color', p(4,:));
        x0 = objective_means; x0(4) = max_param(c2,4);
        plot(max_param(c2,4), objective_function_ND(x0, objective_means, objective_scale), 'o', 'color', p(4,:), 'MarkerEdgeColor', 'k', 'MarkerSize', 10, 'MarkerFaceColor', p(4,:))
        
        x5 = combvec(objective_means(1,1), objective_means(1,2),objective_means(1,3), objective_means(1,4),-4:.01:4)';
        plot(-4:.01:4, objective_function_ND(x5, objective_means, objective_scale), 'color', p(5,:));
        x0 = objective_means; x0(5) = max_param(c2,5);
        plot(max_param(c2,5), objective_function_ND(x0, objective_means, objective_scale), 'o', 'color', p(5,:), 'MarkerEdgeColor', 'k', 'MarkerSize', 10, 'MarkerFaceColor', p(5,:))
        
%         ylim([0 1]);
        xlim([-4 4])
        
        drawnow

    end
    
    if n_burn_in == n_samples
        ESN_error(c1)           = sum(1 - ESN_max_val(c1,:));
        ESN_opt_time(c1)        = toc;
    else
        BAO_error(c1)           = sum(1 - BAO_max_val(c1,:));
        BAO_opt_time(c1)        = toc;
    end
end

%%


%%

close all

subplot(2,3,[1 2 3])
hold on

p_value = anova1([ESN_error'/2 BAO_error'], [],'off');

h = boxplot([ESN_error'/2 BAO_error'], 'notch', 'on','Colors','kk');
plot([1 2], mean([ESN_error'/2 BAO_error']), 'rx', 'MarkerSize', 10, 'LineWidth', 3)

set(h, 'LineWidth'  ,2)
set(h, 'MarkerEdgeColor'  , 'k');

xticklabels({'Echo State Network', 'Bayes-UCB(0.1)'});
ylabel('Cummulative Error')
title(sprintf('P-value = %.2f', p_value))

set(gca,'FontSize', 16)
box off

%%%%%%
subplot(2,3,4)
hold on

h = boxplot(ESN_opt_time(7,:)', 'notch', 'on','Colors','kk', 'Widths', 1);
plot(1, mean(ESN_opt_time(7,:)), 'rx', 'MarkerSize', 10, 'LineWidth', 3)

set(h, 'LineWidth'  ,2)
set(h, 'MarkerEdgeColor'  , 'k');

xticklabels({'Echo State Network'});
ylabel('Seconds / Trial (100 samples)')
yticklabels({'.002', '.004','.006','.008','.010'})

set(gca,'FontSize', 16)
box off

%%%%%%%%
subplot(2,3,[5 6])
hold on
p_value = anova1([ESN_opt_time(7,:)' BAO_opt_time'], [],'off');

h = boxplot([ESN_opt_time(7,:)' BAO_opt_time'], 'notch', 'on','Colors','kk');
plot([1 2], mean([ESN_opt_time(7,:)' BAO_opt_time']), 'rx', 'MarkerSize', 10, 'LineWidth', 3)

set(h, 'LineWidth'  ,2)
set(h, 'MarkerEdgeColor'  , 'k');

xticklabels({'Echo State Network', 'Bayes-UCB(0.1)'});
title(sprintf('P-value = %.2e', p_value))

set(gca,'FontSize', 16)
box off

set(gcf,'Color', 'w')







