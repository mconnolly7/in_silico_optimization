function [opt_data, opt_results] = CNR_experiment(objective_model, input_space, n_trials, n_samples, CNR_list)


mu                      = objective_model.predict(input_space);

contrast                = max(mu) - min(mu);

sigma_list              = contrast./CNR_list;

n_burn_in               = 10;
acquisition_function    = 'UCB';
acq_params              = 0.4;
measurement_function    = [];

for c1 = 1:size(sigma_list,2)
    
    objective_model.noise_sigma = sigma_list(c1);
    
    [X_samples, Y_samples, X_optimal_est, Y_optimal_est, Y_ground_truth] =  ...
        bayes_opt_loop(objective_model, input_space, n_trials, n_samples, n_burn_in, ...
        acquisition_function, acq_params, measurement_function);
    
    opt_data(c1).objective_model     = objective_model;
    opt_data(c1).input_space         = input_space;
    opt_data(c1).sigma               = sigma_list(c1);
    opt_data(c1).n_burn_in          = n_burn_in;
    opt_data(c1).sigma               = sigma_list;
    opt_data(c1).sigma               = sigma_list;
    opt_data(c1).X_samples           = X_samples;
    opt_data(c1).Y_samples           = Y_samples;
    opt_data(c1).X_optimal_est       = X_optimal_est;
    opt_data(c1).Y_optimal_est       = Y_optimal_est;
    opt_data(c1).Y_ground_truth      = Y_ground_truth;
end


sample_number = (2:n_samples)';
for c1 = 1:size(opt_data,2)
    [~, y_max_gt]                           = opt_data(c1).objective_model.discrete_extrema(input_space);
    
    Y_ground_truth                          = opt_data(c1).Y_ground_truth;
    Y_error_trajectory                      = y_max_gt - Y_ground_truth;
    Y_error_trajectory_rel                  = Y_error_trajectory/contrast;
    
    Y_error_rel                             = Y_error_trajectory_rel(end,:); 
    
    [conv_rate_m, conv_rate_ci]             = error_convergence_rate_2(sample_number, Y_error_trajectory_rel(2:end,:));
    
    opt_results.Y_error_rel(:,c1)          	= Y_error_rel;
    opt_results.conv_rate_m(c1)            	= conv_rate_m;
    opt_results.conv_rate_ci(:,c1)         	= conv_rate_ci;
    
end



