function e_dist = error_from_optimum(best_param, optimal_param, gp_model)

best_param          = best_param(end,2:end);
best_Y              = gp_model.predict(best_param);

optimal_Y           = gp_model.predict(optimal_param);

e_dist              = optimal_Y - best_Y;

end

