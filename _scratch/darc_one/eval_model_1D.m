function mean_error = eval_model_1D(P, rc_struct, PLOT, n_trials)

n_samples           = rc_struct.n_samples;
W_in                = rc_struct.W_in;
W                   = rc_struct.W;
W_out               = P(2:end);
leak_rate           = P(1);
input_space         = -4:.001:4;

for c1 = 1:n_trials
    x_state                 = rc_struct.x;

    objective_means         = rand(2,1)*8-4;
    [objective_max, idx]    = max(objective_function(input_space, objective_means));
    optimal_param           = input_space(idx);
    
    input_param             = rand(1) * 8-4 - 1;
    objective_val           = objective_function_noise(input_param, objective_means);
    max_val_noise           = objective_val;
    max_param               = input_param;
    max_val                 = objective_function(input_param, objective_means);
    
    for t = 1:n_samples     
        u                   = [input_param(t); objective_val(t)];
        
        % Get the next parameter
        x_state             = (1-leak_rate) * x_state + leak_rate * tanh(W_in * [1; u] + W * x_state);
        next_param          = W_out * [1; u; x_state];

        input_param(t+1)    = next_param;
        
        % Normalize
        input_param(t+1)    = max(input_param(t+1), -4);
        input_param(t+1)    = min(input_param(t+1), 4);
        
        % Apply next input to the objective function
        objective_val(t+1)              = objective_function_noise(next_param, objective_means);
        
        % Get the max param
        [max_val_noise(t+1), max_val_idx]   = max(objective_val);
        max_param(t+1)                      = input_param(max_val_idx);
        max_val(t+1)                        = objective_function(max_param(t+1), objective_means);
    end
    
    cummulative_error(c1) = sum(objective_max - max_val);
    if any(cummulative_error < 0)
       a = 1; 
    end
end

if PLOT
    subplot(2,1,1)
    plot(max_param)
    hold on
    plot([1 n_samples], [optimal_param optimal_param], 'k--', 'LineWidth', 2)
    subplot(2,1,2)
    plot(objective_max - max_val_noise)
    title(sprintf('Cumm. Error: %.2f', cummulative_error));
end

mean_error = mean(cummulative_error);
end