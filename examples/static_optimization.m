clc; clear; rng(0)

%% % % % % % % %
% Create N-dimensional dataset
% % % % % % % % % 
[X,Y,Z] = peaks(25);
Z       = Z + rand(size(Z))*3; % Add small amount of noise
surf(X,Y,Z);

%% % % % % % % %
% Structure dataset 
% % % % % % % % %
% 
% X - m x N matrix
% Y - m x 1 matrix
% 
x1      = reshape(X,[],1);
x2      = reshape(Y,[],1);
X       = [x1, x2];
Y       = reshape(Z,[],1);

%% % % % % % % %
% Initialize GP to simulate objective surface
% % % % % % % % % 
clc
objective_model = gp_object();
objective_model.initialize_data(X,Y, min(X), max(X));

figure;
objective_model.plot_mean();
hold on
objective_model.plot_standard_deviation();

%% % % % % % % % 
% Set optimization hyperparameters
% % % % % % % % % 
clc;

n_burn_in               = 5;
n_samples               = 50;
aquisition_function     = 3; % Upper-confidence bound
nu                      = 0.6;
params                  = [aquisition_function, nu];

[X_samples, Y_samples, X_optimal_est, Y_ground_truth] ...
    = bayes_opt_on_model(objective_model, n_samples, n_burn_in, params, []);

%% % % % % % % % 
% Plot results
% % % % % % % % %
figure
n_params = size(X_samples,2);

subplot(n_params+1,1,1);
plot(prctile(Y_ground_truth,50,2), '-', 'LineWidth', 2)

for c1 = 1:n_params
    subplot(n_params+1,1,c1+1);
    plot(prctile(X_optimal_est(:,c1),50,2), '-', 'LineWidth', 2)
end
    


subplot(3,1,1)
ylabel('Standardized Theta (4-8Hz)')
xticklabels('')
set(gca,'FontSize', 16)

subplot(3,1,2)
ylabel('Intensity (mW/mm^2)')
xticklabels('')
set(gca,'FontSize', 16)

subplot(3,1,3)
ylabel('Frequency (Hz)')
xticklabels('')
set(gca,'FontSize', 16)

