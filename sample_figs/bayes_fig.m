
rng(1);

[x_gt, y_gt, gp_model] = objective_data();
lower_bound = gp_model.lower_bound;
upper_bound = gp_model.upper_bound;

n_burn_in = 5;

%% Panel 1
set(f, 'currentaxes', ax(3));
gp_model.plot_mean
gp_model.plot_standard_deviation

burn_in_x   = uniform_range(lower_bound, upper_bound, n_burn_in,3);

burn_in_y   = gp_model.sample(burn_in_x);

xx          = [burn_in_x burn_in_x]';
yy          = [-1*ones(size(burn_in_x)) burn_in_y ]';
plot(xx,yy, 'color', 'k');

scatter(burn_in_x, -1*ones(size(burn_in_x)), 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[117,112,179]/255,...
    'LineWidth',2)

scatter(burn_in_x, burn_in_y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[117,112,179]/255,...
    'LineWidth',2)


ylim([-1.5,2])
set(gca,'FontSize', 24)
title('BaO')

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off

%% Panel 2

set(f, 'currentaxes', ax(6));
gp_model.plot_mean
gp_model.plot_standard_deviation

ob_model        = gp_object();
ob_model.initialize_data(burn_in_x, burn_in_y, lower_bound, upper_bound);
ob_model.hyperparameters.cov(2) = -.7;

aq_data                 = -1*ob_model.upper_confidence_bound(x_gt', ob_model, .1) - 1.8;
[max_aq, max_aq_idx]    = max(aq_data);
x_cand                  = x_gt(max_aq_idx);

scatter(burn_in_x, burn_in_y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 .7 .7],...
    'LineWidth',2)

ob_model.plot_mean([117,112,179]/255)
ob_model.plot_confidence_interval([117,112,179]/255)

xx = [x_gt flip(x_gt)];
yy = [-1.5*ones(size(x_gt)) flip(aq_data')];
patch(xx,yy, [117,112,179]/255, 'FaceAlpha', .4)
plot(x_gt, aq_data, 'color', [117,112,179]/255, 'lineWidth', 2);
scatter(x_cand, max_aq+.1, 100, 'Marker', 'v', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
ylim([-1.5,2])


dim = [.83 .59 .3 .3];
str = sprintf('Surrogate Model');
annotation(f, 'textbox', dim, 'String', str, 'FitBoxToText', 'on', ...
    'verticalalignment', 'bottom', 'FontSize', 20, 'Edgecolor', [117,112,179]/255, 'LineWidth', 3);
xa = [.83 .795];
ya = [.59 .56];
annotation('arrow',xa,ya, 'LineWidth', 3, 'color', 'k')

dim = [.83 .4 .3 .3];
str = sprintf('Acquisition Function');
annotation(f, 'textbox', dim, 'String', str, 'FitBoxToText', 'on', ...
    'verticalalignment', 'bottom', 'FontSize', 20, 'Edgecolor', [117,112,179]/255, 'LineWidth', 3);


xa = [.83 .8];
ya = [.4 .38];
annotation('arrow',xa,ya, 'LineWidth', 3, 'color', 'k')

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off

%% Panel 3

set(f, 'currentaxes', ax(9));
gp_model.plot_mean
gp_model.plot_standard_deviation

y_new           = gp_model.sample(x_cand);

ob_model    = gp_object();
ob_model.initialize_data([burn_in_x; x_cand], [burn_in_y; y_new], lower_bound, upper_bound);
ob_model.hyperparameters.cov(2) = -.7;

scatter(x_cand, y_new, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[117,112,179]/255,...
    'LineWidth',2)

aq_data                 = -2*ob_model.upper_confidence_bound(x_gt', ob_model, .1) - 2.1;
[max_aq, max_aq_idx]    = max(aq_data);
x_cand                  = x_gt(max_aq_idx);

ob_model.plot_mean([117,112,179]/255)
ob_model.plot_confidence_interval([117,112,179]/255)

scatter(burn_in_x, burn_in_y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 .7 .7],...
    'LineWidth',2)

yy = [-1.5*ones(size(x_gt)) flip(aq_data')];
patch(xx,yy, [117,112,179]/255, 'FaceAlpha', .4)
plot(x_gt, aq_data, 'color', [117,112,179]/255, 'lineWidth', 2);

scatter(x_cand, max_aq+.1, 100, 'Marker', 'v', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');

ylim([-1.5,2])

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off