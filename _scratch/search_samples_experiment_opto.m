clear

%% Load optogenetic model
load('optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_3D/R1_gamma_3D.mat')

% input_amplitude     = .1:.1:3.5;
% input_frequency     = 2:1:125;
% input_pusle_width   = 60:10:450;
% 
% input_space_RC       = combvec(input_amplitude, input_frequency, input_pusle_width)';


input_amplitude     = 5:1:50;
input_frequency     = 5:.30:42;
input_pusle_width   = 2:.25:10;

input_space_opto    = combvec(input_amplitude, input_frequency, input_pusle_width)';

n_trials            = 100;
n_samples           = 90;
n_subsample         = 100;

acq_function        = 'UCB';
setpoint            = [];


% Subsample the gp_model
n_inputs            = size(input_space_opto,1);
x_subsample         = input_space_opto(randperm(n_inputs,n_subsample),:);         
y_subsample         = gp_model.predict(x_subsample);

gp_model.initialize_data(x_subsample, y_subsample);


%% Random search
acq_params  = .0;
n_burn_in   = n_samples;

rng(0)

for c1 = 1:30
    
    fprintf('%d, ', c1);
    [X_samples_rand(c1,:,:), Y_samples_rand(c1,:), X_optimal_est_rand(c1,:,:), ...
        Y_optimal_est_rand(c1,:), Y_ground_truth_rand(c1,:)] =  ...
        bayes_opt_on_model(gp_model, input_space_opto, n_samples, n_burn_in, acq_function, acq_params, setpoint);
    
end
 fprintf('\n');


 
%% Configured Optimization
acq_params  = .4;
n_burn_in   = 15;

rng(0)

for c1 = 1:30
    
    fprintf('%d, ', c1);
    [X_samples_opt_04(c1,:,:), Y_samples_opt_04(c1,:), X_optimal_est_opt_04(c1,:,:), ...
        Y_optimal_est_opt_04(c1,:), Y_ground_truth_opt_04(c1,:)] =  ...
        bayes_opt_on_model(gp_model, input_space_opto, n_samples, n_burn_in, acq_function, acq_params, setpoint);
    
end
fprintf('\n');


%% Configured Optimization 2
acq_params  = 0;
n_burn_in   = 15;

rng(0)

for c1 = 1:15
    
    fprintf('%d, ', c1);
    [X_samples_opt_1(c1,:,:), Y_samples_opt_1(c1,:), X_optimal_est_opt_1(c1,:,:), ...
        Y_optimal_est_opt_1(c1,:), Y_ground_truth_opt_1(c1,:)] =  ...
        bayes_opt_on_model(gp_model, input_space_opto, n_samples, n_burn_in, acq_function, acq_params, setpoint);
    
end
fprintf('\n');
 
 
%%
% close all
figure
hold on
m   = mean(Y_ground_truth_rand(:,2:end));
s   = std(Y_ground_truth_rand(:,2:end));
se  = s / sqrt(n_trials);
ci  = se*2;

t   = 2:n_samples;
tt  = [t flip(t)];
cc  = [m - ci flip(m + ci)];

patch(tt,cc, 'red', 'FaceAlpha', .2, 'EdgeColor', 'none')
plot(t, m, 'red', 'LineWidth', 2)
% plot(t, max(Y_ground_truth_rand(:,2:end)), 'red', 'LineWidth', 2)

%
m   = mean(Y_ground_truth_opt_04(:,2:end));
s   = std(Y_ground_truth_opt_04(:,2:end));
se  = s / sqrt(n_trials);
ci  = se*2;

t   = 2:n_samples;
tt  = [t flip(t)];
cc  = [m - ci flip(m + ci)];

patch(tt,cc, 'blue', 'FaceAlpha', .2, 'EdgeColor', 'none')
plot(t, m, 'blue', 'LineWidth', 2)
% plot(t, max(Y_ground_truth_opt_04(:,2:end)), 'blue', 'LineWidth', 2)

m   = mean(Y_ground_truth_opt_1(:,2:end));
s   = std(Y_ground_truth_opt_1(:,2:end));
se  = s / sqrt(n_trials);
ci  = se*2;

t   = 2:n_samples;
tt  = [t flip(t)];
cc  = [m - ci flip(m + ci)];

patch(tt,cc, 'g', 'FaceAlpha', .2, 'EdgeColor', 'none')
plot(t, m, 'g', 'LineWidth', 2)
% plot(t, max(Y_ground_truth_opt_04(:,2:end)), 'blue', 'LineWidth', 2)

[a, y_max] = gp_model.discrete_extrema(input_space_opto);
plot([2 n_samples], y_max*[1 1], 'k--', 'LineWidth', 2)








bands = [1 4; 4 10; 11 16; 17 32; 33 50];

for c1 = 1:5
    x_filt(c1,:) = bandpass(x, bands(c1,:), fs);
end




