function example_opt
n_trials    = 10;
n_samples   = 100;
params      = [3 1];
[x, y_meas, objective_model]            = objective_data();

[pos_state, stim_params, optimal_est]   ...
    = bayes_opt_on_model(objective_model, n_trials, n_samples, params);

end

