        function pi = predicted_improvement(P, gp_object, f_max, ksi)
            [ypred, ~, ~, fs] = gp_object.predict(P);
            
            ksi         = ksi  * std(gp_object.y_data);
            z           = (ypred - f_max - ksi)./fs;
            
            pi          = normcdf(z);
            
            pi          = -1*pi;
            pi(pi == 0) = 100;
           
        end