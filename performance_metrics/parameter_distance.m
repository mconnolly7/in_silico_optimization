function  p_dist = parameter_distance(optimal_est, optimal_param, lower_bound, upper_bound)

optimal_est         = optimal_est(end,2:end);

% Normalize parameters 
optimal_est_norm    = linear_normalize_X(optimal_est, lower_bound, upper_bound);
optimal_param_norm  = linear_normalize_X(optimal_param, lower_bound, upper_bound);

p_dist              = norm(optimal_est_norm - optimal_param_norm);

end

