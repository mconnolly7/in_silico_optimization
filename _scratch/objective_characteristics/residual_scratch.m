gp_model = gp_object();
gp_model.initialize_data(X(:,1:2),Y)

subplot(1,2,1)
gp_model.plot_mean

gp_model.minimize(10)

subplot(1,2,2)
gp_model.plot_mean

%%
residuals = gp_model.predict(X(:,1:2)) - Y;
subplot(2,1,1)
scatter(X(:,1),residuals);
subplot(2,1,2)
scatter(X(:,2),residuals);

vartestn(Y,X(:,2))
