function objective_val = objective_function(next_param, objective_means)

objective_val  = normpdf(next_param, objective_means(1), .7) ... 
                    + 0.5*normpdf(next_param, objective_means(2), .7);
                
end
