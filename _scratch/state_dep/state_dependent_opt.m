clear; clc;
data_dir        = 'optogenetic_optimization_project/data/in_silico_data/processed_data/park_2019/iso-awake_static_2019_02_06/';

% psd_data_iso    = [data_dir 'PSD_4_second_sequence_amp-freq_ISO_2019_02_02.mat'];
% psd_data_awake  = [data_dir 'PSD_4_second_sequence_amp-freq_2019_01_24.mat'];

psd_data_iso    = [data_dir 'Mark_4sec_CA3PSD_ISO_freqamp_020619'];
psd_data_awake  = [data_dir 'Mark_4sec_CA3PSD_awake_freqamp_020619.mat'];

delta           = 1:4;
theta           = 4:7;
alpha           = 8:15;
beta            = 16:31;
gamma           = 32:50;
high_gamma      = 50:70;

[X0_awk, X1_awk, U1_awk]    = build_dataset(high_gamma, psd_data_awake);
[X0_iso, X1_iso, U1_iso]    = build_dataset(high_gamma, psd_data_iso);

X0              = [X0_awk; X0_iso];
X1              = [X1_awk; X1_iso];
U1              = [U1_awk; U1_iso];

out_idx         = detect_outlier([X0 X1 U1],5);

X0              = X0(~out_idx,:);
X1              = X1(~out_idx,:);
U1              = U1(~out_idx,:);

state_list      = linspace(min(X0), 3e-10, 50);
% state_list      = [flip(state_list) state_list];
state_list      = [state_list flip(state_list)];
    
dynamics_model  = simulation_gp();
dynamics_model.initialize_data_nonstationary(X0, U1, X1, min(U1), max(U1), state_list);

dynamics_model.gp_model.minimize(10)

n_samples   = 100;
params      = [3 .1];

for trial = 1:30
    fprintf('Trial: %d\n', trial)
    
    % Train BaO
    fprintf('\tTrain BaO\n')
    
    dynamics_model.state_idx = 1;
    [pos_state_bao, stim_params_bao, optimal_hist_bao] =  bayes_opt_on_model(dynamics_model, n_samples, params);
    optimal_bao(trial,:) = optimal_hist_bao(end,2:3);
    

    % Test BaO
    fprintf('\tTest BaO\n')

    dynamics_model.state_idx = 1;
    for c1 = 1:100
        bao_samples(trial,c1) = dynamics_model.sample(optimal_bao(trial,:)); 
    end


%     %% Train SDBO
%     fprintf('\tTrain SDBO\n')
%     dynamics_model.state_idx = 1;
%     [pos_state, stim_params, optimal_sdbo] =  sdbo_opt_on_model(dynamics_model, 100, params);
% 
% 
%     %% Test SDBO
%     fprintf('\tTest SDBO\n')
% 
%     policy(trial) = optimal_sdbo(end);
% 
%     dynamics_model.state_idx = 1;
%     for c1 = 1:100
%         current_state(c1)       = dynamics_model.current_state;
%         opt_stim(c1,:)          = policy(trial).state_extrema(current_state(c1));
%         sdbo_samples(trial,c1)  = dynamics_model.sample(opt_stim(c1,:));
%     end
end

%%
for c1 = 1:30
    [~,~,sdbo_policy(c1,:,:), state_range_sdbo] = calculate_optimal_policy(policy(c1).gp_model, 100);
end
[~,~,optimal_gt, state_range_gt]    = calculate_optimal_policy(dynamics_model.gp_model, 100);


%%
p = [217,95,2
27,158,119]/255;

%%%%%%%
% Plot Performance Trace
%%%%%%%
close all
f           = figure('Position', [81 441 1159 657]);
m_sdbo      = mean(sdbo_samples);
s_sdbo      = std(sdbo_samples);
se_sdbo     = s_sdbo / sqrt(size(sdbo_samples,1));
ci_sdbo     = se_sdbo * 1.96;

sdbo_25     = prctile(sdbo_samples,25);
sdbo_50     = prctile(sdbo_samples,50);
sdbo_75     = prctile(sdbo_samples,75);


m_bao       = mean(bao_samples);
s_bao       = std(bao_samples);
se_bao      = s_bao / sqrt(size(bao_samples,1));
ci_bao      = se_bao * 1.96;

bao_25      = prctile(bao_samples,25);
bao_50      = prctile(bao_samples,50);
bao_75      = prctile(bao_samples,75);

t           = 1:size(state_list,2);
tt_policy          = [t flip(t)];

subplot(2,2,[1 3])

yy_sdbo     = [m_sdbo-ci_sdbo flip(m_sdbo + ci_sdbo)];
patch(tt_policy, yy_sdbo, p(1,:), 'FaceAlpha', .3, 'EdgeColor', 'none', 'linewidth', 2);

hold on
plot(t,m_sdbo,'Color', p(1,:), 'linewidth', 2)

yy_bao     = [m_bao-ci_bao flip(m_bao + ci_bao)];

patch(tt_policy, yy_bao, p(2,:), 'FaceAlpha', .3, 'EdgeColor', 'none', 'linewidth', 2);
hold on
plot(t,m_bao,'Color', p(2,:), 'linewidth', 2)
plot(t, state_list, 'color', 'k', 'LineWidth', 2);

xlabel('Sample #')
ylabel('Gamma Power')
set(gca,'FontSize', 16);

legend({'SDBO', '', 'BaO', '', 'X_{k0}'}, 'box', 'off')

%%%%%%%
% Plot Policy 1
%%%%%%%

policy_1    = squeeze(sdbo_policy(:,:,1));
policy_1_25 = prctile(policy_1, 25);
policy_1_50 = prctile(policy_1, 50);
policy_1_75 = prctile(policy_1, 75);

static_1_25 = prctile(optimal_bao(:,1), 25);
static_1_50 = prctile(optimal_bao(:,1), 50);
static_1_75 = prctile(optimal_bao(:,1), 75);

tt_policy   = [state_range_sdbo flip(state_range_sdbo)];
yy_policy   = [policy_1_25 flip(policy_1_75)];

t_static    = [min(state_range_sdbo)  max(state_range_sdbo)];
tt_static   = [t_static flip(t_static)];

y_static    = [static_1_25 static_1_75];
yy_static   = [static_1_25 static_1_25 static_1_75 static_1_75];


subplot(2,2,2)
hold on
patch(tt_policy, yy_policy, p(1,:), 'FaceAlpha', .3, 'EdgeColor', 'none', 'linewidth', 2)
plot(state_range_sdbo, policy_1_50, 'color', p(1,:), 'linewidth', 2)
patch(tt_static, yy_static, p(2,:), 'FaceAlpha', .3, 'EdgeColor', 'none', 'linewidth', 2)
plot(t_static, [static_1_50 static_1_50] , 'Color', p(2,:), 'LineWidth', 2)



plot(state_range_gt, optimal_gt(:,1), 'Color', 'k', 'LineWidth', 2)

xticklabels([])
xlim([min(state_list) max(state_list)])
ylabel('Intensity (mW/mm^2)')
ylim([min(U1(:,1)) max(U1(:,1))])
set(gca,'FontSize', 16);

%%%%%%%
% Plot Policy 2
%%%%%%%
policy_2    = squeeze(sdbo_policy(:,:,2));
policy_2_25 = prctile(policy_2, 25);
policy_2_50 = prctile(policy_2, 50);
policy_2_75 = prctile(policy_2, 75);

static_2_25 = prctile(optimal_bao(:,2), 25);
static_2_50 = prctile(optimal_bao(:,2), 50);
static_2_75 = prctile(optimal_bao(:,2), 75);

tt_policy   = [state_range_sdbo flip(state_range_sdbo)];
yy_policy   = [policy_2_25 flip(policy_2_75)];

t_static    = [min(state_range_sdbo)  max(state_range_sdbo)];
tt_static   = [t_static flip(t_static)];

y_static    = [static_2_25 static_2_75];
yy_static   = [static_2_25 static_2_25 static_2_75 static_2_75];

legend({'SDBO', '','BaO', '',  'Ground Truth'}, 'Location', 'southeast', 'box', 'off')
subplot(2,2,4)
hold on

patch(tt_static, yy_static, p(2,:), 'FaceAlpha', .3, 'EdgeColor', 'none', 'linewidth', 2)
plot(t_static, [static_2_50 static_2_50] , 'Color', p(2,:), 'LineWidth', 2)

patch(tt_policy, yy_policy, p(1,:), 'FaceAlpha', .3, 'EdgeColor', 'none', 'linewidth', 2)
plot(state_range_sdbo, policy_2_50, 'Color', p(1,:), 'LineWidth', 2)

plot(state_range_gt, optimal_gt(:,2), 'Color', 'k', 'LineWidth', 2)

xlim([min(state_list) max(state_list)])
ylim([min(U1(:,2)) max(U1(:,2))])
ylabel('Frequency (Hz)')
xlabel('Baseline Gamma Power')
set(gca,'FontSize', 16);


x_coords        = [.06 .5];
y_coords        = [.88  .45 ];
sublabel_size   = 30;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','C','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);


print( 'sdbo.png','-dpng', '-r500')




%%

close all
set(gca,'XColor', 'none','YColor','none')

hold on
t       = 1:1000;
t_end   = 0;
amp     = 8;
fs      = 130;
period  = 1000/fs;
color   = 0*[1 1 1];

% fig 1
amp_(1)             = 4;
fs_(1)              = 130;
color_scale_(1)     = 0;

% fig 2
amp_(1)             = 8;
fs(1)               = 130;
color_scale_(1)     = 0;

% fig 3
amp_(1)             = 8;
fs_(1)              = 220;
color_scale_(1)     = 0;


% fig 4
amp_(1)             = 3;
fs_(1)              = 220;
color_scale_(1)     = 0;


for c2 = 1:size(amp_,2)

    amp     = amp_(c2);
    fs      = fs_(c2);
    color   = color_scale_(c2)*[1 1 1];
    period  = 1000/fs;

    for t_start = 3:period:25
        plot([t_end t_start], [0 0], 'color', color, 'linewidth', 2)
        plot([t_start t_start], [0 amp], 'color', color, 'linewidth', 2)
        plot([t_start t_start+.06], [amp amp], 'color', color, 'linewidth', 2)
        plot([t_start+.06 t_start+.06], [amp 0], 'color', color, 'linewidth', 2)
        plot([t_start+.06 t_start+.13], [0 0], 'color', color, 'linewidth', 2)
        plot([t_start+.13 t_start+.13], [0 -1*amp/8], 'color', color, 'linewidth', 2)
        plot([t_start+.13 t_start+.61], [-1*amp/8  -1*amp/8], 'color', color, 'linewidth', 2)
        plot([t_start+.61 t_start+.61], [-1*amp/8 0], 'color', color, 'linewidth', 2)
        t_end = t_start+.61;
    end
    plot([t_start+.61 t_start+10], [0 0], 'color', color, 'linewidth', 2)
    ylim([-4 10])
end