function discrete_vol = get_discrete_volume(Y1,Y2,ref)
dy1 = .5;
dy2 = .5;

% grid_1 = ref(1):dy1:max(Y1);
% grid_2 = min(Y2):dy2:ref(2);

% Speed hack
% if size(grid_1,2) > 200
    grid_1 = linspace(ref(1),max(Y1),200);
    dy1 = grid_1(2) - grid_1(1);
% end

% if size(grid_2,2) > 200
    grid_2 = linspace(min(Y2),ref(2),200);
    dy2 = grid_2(2) - grid_2(1);
% end

output_space    = combvec(grid_1, grid_2)';
output_vol      = zeros(size(output_space,1),1);

for c1 = 1:size(output_space,1)
    
    d_output = output_space(c1,:);
    
    for c2 = 1:size(Y1,1)
        if d_output(1) < Y1(c2) && d_output(2) > Y2(c2)  
            output_vol(c1) = 1;
            break
        end      
    end
end

discrete_vol = sum(output_vol) * dy1 * dy2;
end

