function [pos_state, stim_params] = random_opt(objective_model, n_trials)

for c1 = 1:n_trials
    [pos_state(:,c1), stim_params(:,:,c1)] = random_opt_opt(objective_model);
end

end


function [pos_state, stim_params] = random_opt_opt(objective_model)


p_duration  =  polyfit([0 1], [15 120],1);
p_amplitude =  polyfit([0 1], [1 4],1);

n_learning  = 100;
n_ghost     = 100;
for c1 = 1:n_learning    
    stim_params(c1,1)   = polyval(p_duration, rand);
    stim_params(c1,2)   = polyval(p_amplitude, rand);
    
    pos_state(c1,1)     = objective_model.sample(stim_params(c1,:));
end

for c1 = n_learning+1:n_learning+n_ghost    
    stim_params(c1,1)   = polyval(p_duration, rand);
    stim_params(c1,2)   = polyval(p_amplitude, rand);
    
    pos_state(c1,1)     = objective_model.sample(stim_params(c1,:));
end
end


