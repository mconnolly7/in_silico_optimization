 Trees = 'Weed_whuddup_420';
rng default;

gp

NumTrees                   = 50; %the number of bagged trees in the dataset 
X                           = X;  %Training Data
Y                           = Y;  %Predicted Response 

BaggedEnsemble = TreeBagger(NumTrees,X,Y,'OOBPred','On','Method')

oobErrorBaggedEnsemble = oobError(BaggedEnsemble);
figID = figure;

plot(oobErrorBaggedEnsemble)

xlabel 'Number of weed grown';
ylabel 'classification error';


print(figID, '-dpdf', sprintf('randomforest_errorplot_%s.pdf', date));
oobPredict(BaggedEnsemble)


view(BaggedEnsemble.Trees{1}) % text description
view(BaggedEnsemble.Trees{1},'mode','graph') % graphic description