function [tt,z,optimal_x, state_range] = calculate_optimal_policy(dynamics_model, steps)

state_min   = dynamics_model.lower_bound(1);
state_max   = dynamics_model.upper_bound(1);

param_min   = dynamics_model.lower_bound(2:end);
param_max   = dynamics_model.upper_bound(2:end);

tt          = linspace(param_min(1), param_max(1), 100);
tt          = combvec(tt, linspace(param_min(2), param_max(2), 100))';

state_range = linspace(state_min, state_max, steps);

for c1 = 1:size(state_range,2)
    x_candidate     = [repmat(state_range(c1), size(tt,1), 1) tt];
    z(:,c1)         = dynamics_model.predict(x_candidate);
    [~, max_idx]    = max(z(:,c1));
    optimal_x(c1,:) = tt(max_idx,:);
end
end

