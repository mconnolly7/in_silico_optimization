close all
clear

data_dir        = 'data/optogenetic_data/park_2019/rest-awake_static_2019_02_27/';
% data_dir        = 'processed_data/optogenetic_data/dynamic_opt/PSD_ISO_awake_freq_amp_2019_02_06/';

% psd_data_rest   = [data_dir 'PSD_4_second_sequence_amp-freq_ISO_2019_02_02.mat'];
% psd_data_awake  = [data_dir 'PSD_4_second_sequence_amp-freq_2019_01_24.mat'];

% psd_data_rest    = [data_dir 'Mark_4sec_CA1PSD_ISO_freqamp_020619'];
% psd_data_awake  = [data_dir 'Mark_4sec_CA1PSD_awake_freqamp_020619.mat'];

psd_data_rest   = [data_dir 'GS2PSDCA3.mat'];
psd_data_awake  = [data_dir 'GS1PSDCA3.mat'];

feature_z       = 0;

delta           = 1:4;
theta           = 4:7;
alpha           = 8:15;
beta            = 16:31;
gamma           = 32:50;
high_gamma      = 50:70;
% f_bands_in      = {high_gamma};
f_bands_in      = {delta, theta, alpha, beta, gamma};
% f_bands_out     = {high_gamma};
f_bands_out     = {delta, theta, alpha, beta, gamma};

K_folds     = 10;

X0 = [];
X1 = [];
U1 = [];

% for c1 = 1:size(f_bands_in,2)
%     f_band              = f_bands_in{c1};
%     X0_PSD_ISO(:,c1) 	= build_dataset(f_band, psd_data_iso);
% %     X0_COH_ISO(:,c1)    = build_dataset(f_band, coh_data_iso);
%     X0_PSD_AWK(:,c1)    = build_dataset(f_band, psd_data_awake);
% %     X0_COH_AWK(:,c1)    = build_dataset(f_band, coh_data_awake);
% end


% X0_all = [X0_PSD_ISO; X0_PSD_AWK];


for c1 = 1:size(f_bands_out,2)
    
    fprintf('Output Band: %d\n',c1);
    f_band                  = f_bands_out{c1};
    
    [X0_ISO, X1_ISO, U1_ISO]     = build_dataset(f_band, psd_data_rest);
    [X0_AWK, X1_AWK, U1_AWK]     = build_dataset(f_band, psd_data_awake);
    
    X0                      = [X0_ISO; X0_AWK];
    U1                      = [U1_ISO; U1_AWK];
    X1                      = [X1_ISO; X1_AWK];
    
    inc_idx                 = find(U1(:,1) < 300);
    out_idx                 = detect_outlier([X0 X1 U1],5);
    
    X0                      = X0(~out_idx & inc_idx,:);
    X1                      = X1(~out_idx & inc_idx,:);
    U1                      = U1(~out_idx & inc_idx,:);
    
    if feature_z 
        X0 = zscore(X0);
        X1 = zscore(X1);
        U1 = zscore(U1);
    end
    
    [~, static_mse]         = cross_validate_gp(U1, X1, K_folds);
    [~, state_mse]          = cross_validate_gp(X0, X1, K_folds);
    [~, joint_mse]          = cross_validate_gp([X0, U1], X1, K_folds);
    
    static_mse_norm(c1,:)   = static_mse / var(X1);
    state_mse_norm(c1,:)    = state_mse  / var(X1);
    joint_mse_norm(c1,:)    = joint_mse  / var(X1);

end

%%
anova1([static_mse_norm(1,:); state_mse_norm(1,:); joint_mse_norm(1,:)]')
%%
p = [27,158,119
117,112,179
217,95,2]/255;

close all

Y       = [mean(static_mse_norm,2) mean(state_mse_norm,2) mean(joint_mse_norm,2)];
Y_std   = [ std(static_mse_norm,[],2) std(state_mse_norm,[],2) std(joint_mse_norm,[],2)];
Y_se    = Y_std / sqrt(10);

[h b] = barwitherr(Y_std, Y);
xticklabels({'Delta (1-4Hz)','Theta (4-7Hz)','Alpha (8-15Hz)','Beta (16-31Hz)','Gamma (32-50Hz)'}); 

s1 = sprintf('%-7s %-26s', 'Static:', 'X_{k1} = F(U_{k1})');
s2 = sprintf('%-7s %-26s', 'State:', 'X_{k1} = F(X_{k0})');
s3 = sprintf('%-7s %-26s', 'Joint:', 'X_{k1} = F(X_{k0}, U_{k1})');

ylabel('NMSE')
set(gca, 'FontSize', 14);
xtickangle(25);

for c1 = 1:3
    set(h(c1),'FaceColor',p(c1,:));
    set(h(c1),'LineWidth', 2)
    set(h(c1),'EdgeColor','k');
    set(b(c1),'LineWidth', 2)
end

hold on

plot([5 5.222],[1.0 1.0], 'k', 'LineWidth',2);
plot(mean([5 5.222]), 1.05, 'r*', 'MarkerSize', 8);

plot([4.778 5.222],[1.1 1.1], 'k', 'LineWidth',2);
plot(mean([4.778 5.222]), 1.15, 'r*', 'MarkerSize', 8);

legend(h,{s1, s2, s3}, 'location', 'northwest') 
