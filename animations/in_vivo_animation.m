% clear

%% Load optogenetic model
load('optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_2D/R3_gamma.mat')

%% Load in vivo data
paper_dir               = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
experiment_dir          = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/'];
optimization_data_dir   = [experiment_dir 'optimization_data/'];
raw_data_dir            = [optimization_data_dir 'raw_data/'];
processed_data_dir      = [optimization_data_dir 'processed_data/'];

% data_dir            = 'optogenetic_optimization_project/data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/';
subject_dirs        = {'EPI033-GP' 'EPI032-GP'};

index = 1;
for c1 = 1:size(subject_dirs,2)
    subject_path    = [processed_data_dir subject_dirs{c1}];
    d               = dir(subject_path);
    
    for c2 = 1:size(d,1)        
        experiment_dir = d(c2).name;
        
        if ~strcmp(experiment_dir(1),'.') && d(c2).isdir
           experiment_path              = [subject_path filesep experiment_dir];
          
           load([experiment_path filesep 'gp_model.mat'])
           
           [x_max_est, y_max_est]       = get_optimization_trajectory(gp_temp);
           
           plot_data(index).x_sample    = gp_temp.x_data;
           plot_data(index).y_sample    = gp_temp.y_data;
           
           plot_data(index).x_max_est   = x_max_est;
           plot_data(index).y_max_est   = y_max_est;
           
           index = index + 1;
           
        end
    end
end

%%
input_amplitude     = 5:1:50;
input_frequency     = 5:1:42;

input_space_opto    = combvec(input_amplitude, input_frequency)';

intensity_map       = [43.8782, -169.9840
                    48.4936, -184.5139];
   
p = [215,25,28
    43,131,186
    171,217,233]/255;

n_trials            = 10;
n_samples           = 30;
n_subsample         = 1000;

acq_function        = 'UCB';
setpoint            = [];

% Subsample the gp_model
n_inputs            = size(input_space_opto,1);
x_subsample         = input_space_opto(randperm(n_inputs,n_subsample),:);         
y_subsample         = gp_model.predict(x_subsample);

gp_model.initialize_data(x_subsample, y_subsample);
gp_model.lower_bound = [5 5];
gp_model.upper_bound = [50 42];
gp_model.update_plot_inputs;

%%
clear X_est
clear Y_est

for c1 = 1:size(plot_data,2)
     X_est(c1,:,:) = plot_data(c1).x_max_est(:,[1 2]);    
    
    if c1 > 3
       X_est(c1,:,1) = X_est(c1,:,1) * intensity_map(1,1) + intensity_map(1,2);
    else
       X_est(c1,:,1) = X_est(c1,:,1) * intensity_map(2,1) + intensity_map(2,2);
    end
    
    Y_est(c1,:) = gp_model.predict(squeeze(X_est(c1,:,:)));
end

close all

f = figure('Position', [1 181 1137 870]);
set(gcf,'color','w');

gp_model.plot_mean
colormap(viridis)

xlabel('Amplitude (mW/mm^2)')
ylabel('Frequency (Hz)')
zlabel('Gamma Power (33-50 Hz)')
set(gca, 'FontSize', 22)
xlim([5 50])
ylim([5 42])

index = 1;
for c1 = 1:size(X_est,1)
    
    for c2 = 3:n_samples
               
        cla
        
        gp_model.plot_mean
        colormap(viridis)
        view(-71,35)
        % Plot the previous trials
        for c3 = 1:c1-1
            for c4 = 3:n_samples
                x1      = X_est(c3,c4-1,1);
                y1      = X_est(c3,c4-1,2);
                x2      = X_est(c3,c4,1);
                y2      = X_est(c3,c4,2);
                
                if x1 == x2
                    x_seg = x1*ones(1,30);
                    y_seg = linspace(y1,y2,30);
                else
                    coeffs  = polyfit([x1 x2], [y1 y2],1);
                    m       = coeffs(1);
                    b       = coeffs(2);
                    
                    x_seg   = linspace(x1,x2,30);
                    y_seg   = polyval(coeffs, x_seg);
                end
                z_seg   = gp_model.predict([x_seg; y_seg]')+0.001;
                plot3(x_seg, y_seg, z_seg, '-', 'color', .5*[1 1 1], 'LineWidth',2)
            end
            
            if c3 > 3
                scatter3(X_est(c3,end,1),X_est(c3,end,2),Y_est(c3,end), 100, p(1,:),'filled')
            else
                scatter3(X_est(c3,end,1),X_est(c3,end,2),Y_est(c3,end), 100, p(2,:),'filled')
            end
            
        end
        
        % Plot each new sample
        X_t         = squeeze(X_est(c1,2:c2,:));
        Y_t         = Y_est(c1,2:c2);
        
        for c3 = 2:size(X_t,1)
            x1      = X_t(c3-1,1);
            y1      = X_t(c3-1,2);
            x2      = X_t(c3,1);
            y2      = X_t(c3,2);
            
            if x1 == x2
               x_seg = x1*ones(1,30);
               y_seg = linspace(y1,y2,30);
            else
                coeffs  = polyfit([x1 x2], [y1 y2],1);
                m       = coeffs(1);
                b       = coeffs(2);

                x_seg   = linspace(x1,x2,30);
                y_seg   = polyval(coeffs, x_seg);
            end
            z_seg   = gp_model.predict([x_seg; y_seg]')+0.001;
            plot3(x_seg, y_seg, z_seg, 'k-', 'LineWidth',2)
        end
        
        if c1 > 3
            scatter3(X_t(end,1),X_t(end,2),Y_t(end), 100, p(1,:),'filled')
        else
            scatter3(X_t(end,1),X_t(end,2),Y_t(end), 100, p(2,:),'filled')
        end
        
        drawnow
        F               = getframe(gcf);
        [X, Map]        = frame2im(F);
        X_frames{index} = X;
        index           = index+1;
    end
    
end
    
%%
v = VideoWriter('in_vivo_UCB_0_4.mp4', 'MPEG-4');
v.FrameRate = 5;
open(v)
for c1 = 1:size(X_frames,2)
   writeVideo(v,X_frames{c1});
end
close(v)

