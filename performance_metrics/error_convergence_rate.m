function e_rate = error_convergence_rate(sample_number, est_optimal_y, gt_optimal_y)

if nargin == 3
    distance_y          = abs(est_optimal_y - gt_optimal_y);
else
    distance_y          = est_optimal_y;
end

for c1 = 1:size(est_optimal_y,2)
    f                   = fit(sample_number, distance_y(:,c1), 'exp1', 'Startpoint',[0 0]);
    e_rate(c1)          = f.b;
end

end

