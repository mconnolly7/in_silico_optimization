function auc = pareto_roc(data_struct, ep_model, emg_model,color)

input_cathode               = 0:3;
input_anode                 = 0:3;
input_amplitudes            = .1:.25:5;
input_space                 = combvec(input_cathode, input_anode, input_amplitudes)';

% Generate ground truth pareto idx for real-time error plotting
f1                          = ep_model.predict(input_space);
f2                          = emg_model.predict(input_space);

% Estimate Pareto front
costs_est                   = [f1 f2];
[~, dom_order_gt]           = get_pareto_2(1,-1, costs_est);
    
dom_order_est               = data_struct.dom_order_est;

for c2 = 1:size(dom_order_est,2)
    
    dom_order_sample = squeeze(dom_order_est(:,c2,:));
    
    for c1 = 1:100
        max_dom_order   = c1-2;

        pos_est                     = dom_order_sample  <= max_dom_order;
        neg_est                     = dom_order_sample  > max_dom_order;

        pos_gt                      = dom_order_gt == 0;
        neg_gt                      = dom_order_gt > 0;

        pareto_true_pos             = pos_est & pos_gt;
        pareto_false_pos            = pos_est & neg_gt;
        pareto_true_neg             = neg_est & neg_gt;
        pareto_false_neg            = neg_est & pos_gt;

        precision                   = sum(pareto_true_pos) ./ (sum(pareto_true_pos) + sum(pareto_false_pos));
        recall                      = sum(pareto_true_pos) ./ (sum(pareto_true_pos) + sum(pareto_false_neg));
        specificity                 = sum(pareto_true_neg) ./ sum(neg_gt);

        recall_all(:,c1,c2)         = recall;
        specificity_all(:,c1,c2)    = specificity;


    end
end
% hold on
% plot(1-specificity_all', recall_all', 'k')
% 
% plot(mean(1-specificity_all), mean(recall_all), 'r')

auc = zeros(size(specificity_all,1),size(specificity_all,3));

for c1 = 1:size(specificity_all,1) % trial
    
    for c3 = 1:size(specificity_all,3) % sample
        
        for c2 = 1:size(specificity_all,2)-1 % threshold

            x1 = 1-specificity_all(c1,c2,c3);
            x2 = 1-specificity_all(c1,c2+1,c3);

            y1 = recall_all(c1,c2,c3);
            y2 = recall_all(c1,c2+1,c3);

            c = x2-x1;
            auc(c1,c3) = auc(c1,c3) + c * (y1 + y2)/2;
        end
    end
end
hold on
% plot(auc', 'color', .5*[1 1 1], 'LineWidth', 1)
% plot(auc', 'color', 'b', 'LineWidth', 1)

m = mean(auc);
s = std(auc);
se = s/sqrt(30);
ci = se * 1.96;

% plot(m, 'r', 'LineWidth', 3)
plot(m, 'color', color, 'LineWidth', 3)
% plot([m+s; m-s]', 'color', color,'LineWidth', 2)
% plot([m+ci; m-ci]', 'color', color, 'LineWidth', 2)
% plot([min(auc)]', 'k', 'LineWidth', 1)
% 
% hold on
% plot([1 100], [0.5 0.5], 'k--', 'LineWidth',2)
% plot(prctile(auc,25), 'k', 'LineWidth', 1)
% plot(prctile(auc,75), 'k', 'LineWidth', 1)
% plot(prctile(auc,0), 'color', color)
% plot(prctile(auc,100), 'k')
xlabel('Sample #')
ylabel('AUC')
set(gca,'FontSize', 16)
end



