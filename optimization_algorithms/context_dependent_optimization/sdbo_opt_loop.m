function [pos_state, stim_params, optimal_est] = sdbo_opt_loop(objective_model, trials, n_samples, params)

for c1 = 1:trials
    fprintf('\tTrial %d\n', c1);
    [pos_state(:,c1), stim_params(:,:,c1), optimal_est(:,:,c1)] =  sbao_opt_loop(objective_model, n_samples, params);
end

end


