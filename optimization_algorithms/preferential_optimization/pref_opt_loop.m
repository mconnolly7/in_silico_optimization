function [estimated_x_optimal, estimated_g_optimal, estimated_g_cnr_optimal, x, xp] = ...
    pref_opt_loop(objective, x_input, nu, PLOT, n_burn_in, n_samples, n_trials)


[g_mean, g_std]             = objective.g_model.predict(x_input);
[g_mean_min, g_min_idx]     = min(g_mean);
g_std_min                   = g_std(g_min_idx);
lower_bound                 = min(x_input);
upper_bound                 = max(x_input);

%
% Run optimization trials on objective
%
for trial = 1:n_trials
    fprintf('\tTrial: %d\n', trial);
    
    %
    % draw burn in samples
    %
    x               = uniform_range(lower_bound, upper_bound, n_burn_in);
    xp              = uniform_range(lower_bound, upper_bound, n_burn_in);
    
    x_data          = [x xp];
    y_data          = objective.sample(x, xp);
    x_condorcet     = [];
    g_data          = [];
    g_cnr           = [];
    
    for c1 = n_burn_in+1:n_samples
        fprintf('\t\tSample: %d\n', c1);
        
        %
        % Fit classifier gp with kernel k to Dj  and learn pi_f,j(x)
        %
        % - Need to add hyperpriors for model fitting
        %
        gp_classifier = gpc_model();
        gp_classifier.initialize_data(x_data, y_data, [-1 -1], [1 1]);
        
        
        %
        % Compute Copeland score
        %
        for c2 = 1:size(x_input,1)
            x_s         = repmat(x_input(c2,:), size(x_input,1), 1);
            [p,~,~,s]   = gp_classifier.predict([x_s, x_input]);
            
            p_sum(c2)   = mean(p);
            s_sum(c2)   = mean(s);
        end
        

        %
        % Trying UCB acquisition
        %
        t = c1;
%         subplot(3,1,1)
%         plot(reshape(x_input(:,2),4,20)', reshape(p_sum,4,20)','o-');
%         subplot(3,1,2)
%         plot(reshape(x_input(:,2),4,20)', reshape(s_sum,4,20)','o-');
%         subplot(3,1,3)

        ucb     = p_sum - log(t) * s_sum * nu; 
%         plot(reshape(x_input(:,2),4,20)', reshape(ucb,4,20)','o-');
%         drawnow
        
        
        
        %
        % Select x_new based on UCB and 
        %
        [~, x_max_idx]      = min(ucb);
        x_new               = x_input(x_max_idx,:);
         
        
        %
        % Save the Condercet winner
        %
        [~, x_max_idx]      = min(p_sum);
        x_condorcet(c1,:)   = x_input(x_max_idx,:);
        
        
        %
        % Select the xp_new that maximizes the variance with x_new
        %
        x_s                 = repmat(x_new, size(x_input,1), 1);
        [~,~,~,s]           = gp_classifier.predict([x_input, x_s]);
        
        [~, xp_idx]         = max(s);
        xp_new              = x_input(xp_idx,:);
        
        x_data_new          = [x_new xp_new];
        
        
        %
        % Sample duel and update_model
        %
        y_data_new          = objective.sample(x_new, xp_new);
        g_data(c1)          = objective.g_model.predict(x_condorcet(c1,:));
        
        g_cnr(c1)           = (g_data(c1) - g_mean_min)/g_std_min;

        x_data              = [x_data; x_data_new];
        y_data              = [y_data; y_data_new];
        
        
        %
        % Plotting
        %
        if PLOT
            subplot(3,2,1)
            objective.plot_mean
            title('Unknown Function G')
            set(gca,'FontSize', 16)
            hold off
                        
            if size(x_input,2) == 2
                subplot(3,2,3)
                plot(reshape(x_input(:,2),4,20)', reshape(p_sum,4,20)','o-');
                title('x_{new} = argmax_x Copeland(x)')
                set(gca, 'FontSize', 16);
                
                subplot(3,2,5)
                plot(reshape(x_input(:,2),4,20)', reshape(s,4,20)','o-');
                title('xp_{new} = argmax_{xp} uncertainty(xp|x)');
                set(gca, 'FontSize', 16);
                
                subplot(3,2,2)
                hold off
                plot(x_data(:,1), 'k--', 'LineWidth', 2)
                hold on
                plot(x_data(:,3), '--', 'color', .7*[1 1 1], 'LineWidth', 2)
                hold on
                plot(x_condorcet(:,1), 'r', 'LineWidth', 2)
                xlim([0 n_samples])
                legend({'x', 'x''', 'x*'});
                title('Measured Samples')
                ylabel('Contact');
                set(gca, 'FontSize', 16);
                
                subplot(3,2,4)
                hold off
                plot(x_data(:,2), 'k--', 'LineWidth', 2)
                hold on
                plot(x_data(:,4), '--',  'color', .7*[1 1 1],'LineWidth', 2)
                hold on
                plot(x_condorcet(:,2), 'r', 'LineWidth', 2)
                xlim([0 n_samples])
                legend({'x', 'x''', 'x*'});
                title('Measured Samples')
                ylabel('Amplitude (Volts)');
                set(gca, 'FontSize', 16);
                
                subplot(3,2,6)
                hold off
                idx = n_burn_in+1:c1;
                plot(idx, g_cnr(idx), 'r', 'LineWidth', 2)
                xlim([0 n_samples])
                ylim([0 4])
                legend({'(g-min_g)/g_{std}'});
                title('Measured Samples')
                ylabel('CNR');
                set(gca, 'FontSize', 16);
                               
            elseif size(x_input,2) == 1
                subplot(3,2,3)
                scatter(x_input(:,1), p_sum);
                title('x_{new} = argmax_x Copeland(x)')
                set(gca, 'FontSize', 16);
                
                subplot(3,2,5)
                scatter(x_input(:,1), s)
                title('xp_{new} = argmax_{xp} uncertainty(xp|x)');
                set(gca, 'FontSize', 16);
                
                subplot(3,2,4)
                gp_classifier.plot_confidence_magnitude
                title('Uncertainty')
                set(gca, 'FontSize', 16);
                
                subplot(3,2,2)
                hold off
                gp_classifier.plot_mean;
                hold on
                scatter3(x_data(:,1), x_data(:,2), y_data)
                view([0 90])
                title('Preference Function \pi')
                xlabel('X')
                ylabel('X''')
                set(gca, 'FontSize', 16);
                
                subplot(3,2,6)
                hold off
                plot(x_data(:,1), 'r')
                hold on
                plot(x_data(:,2), 'k--')
                xlim([0 n_trials])
                legend({'x', 'xp'});
                title('Measured Samples')
                set(gca, 'FontSize', 16);
            end
            
            drawnow
        end
        
    end
    
    subplot(3,2,5)
    cla
    
    x_data_all(trial,:,:)               = x_data;
    y_data_all(trial,:)                 = y_data;
    estimated_x_optimal(trial,:,:)      = x_condorcet;
    estimated_g_optimal(trial,:)        = g_data;
    estimated_g_cnr_optimal(trial,:)    = g_cnr;
    
    
end
x                   = x_data_all(:,:, [1 2]);
xp                  = x_data_all(:,:, [3 4]);

end
