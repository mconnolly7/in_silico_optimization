function X_sample = expected_hypervolume_improvement(gp1, gp2, input_space, ref, model_function, exploration_param, hyperprior, hyperprior_config)

volume_cand         = 0;
input_optimal_est   = [];

for c1 = 1:100
    if  c1 <= 20 || std(volume_cand) == 0
        input_sample(c1,:)      = input_space(randi(size(input_space,1)),:);
        
    else
        objective_est   = gp_object();
        objective_est.initialize_data(input_sample, volume_cand)
            
        input_sample(c1,:)      	= bayes_opt_update(objective_est, input_space, 'UCB', 10, c1);
        input_optimal_est(c1,:)     = objective_est.discrete_extrema(input_space);
    
        if all(std(input_optimal_est(c1-20:c1,:)) < .01) % Check for early convergence
            break;
        end
    end
    
    volume_cand(c1,1)     = test_input(input_sample(c1,:), input_space, gp1,gp2, ref, model_function, exploration_param, hyperprior, hyperprior_config);
end

if ~isempty(input_optimal_est)
    X_sample = input_optimal_est(end,:);
else
    [~, opt_idx]    = max(volume_cand); 
    X_sample        = input_sample(opt_idx,:);
end

end

function volume_cand = test_input(input, input_space, gp1,gp2, ref, exploration_param, model_function, hyperprior, hyperprior_config)

[Y1_m,~,~,Y1_s]     = gp1.predict(input);
[Y2_m,~,~,Y2_s]     = gp2.predict(input);

X_cand              = [gp1.x_data; input];
Y1_cand             = [gp1.y_data; Y1_m + exploration_param*Y1_s];
Y2_cand             = [gp2.y_data; Y2_m - exploration_param*Y2_s];

gp1_cand            = model_function{1}(X_cand, Y1_cand, hyperprior{1}, hyperprior_config);
gp2_cand            = model_function{2}(X_cand, Y2_cand, hyperprior{2}, hyperprior_config);

Y1_est_cand         = gp1_cand.predict(input_space);
Y2_est_cand         = gp2_cand.predict(input_space);

pareto_idx_cand     = get_pareto_2(1, -1, [Y1_est_cand Y2_est_cand]);
volume_cand         = get_discrete_volume(Y1_est_cand(pareto_idx_cand),Y2_est_cand(pareto_idx_cand),[0 max(Y2_cand)]);

end

