load('optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_2D/R1_gamma.mat')

input_amplitude     = 5:1:50;
input_frequency     = 5:.30:42;

input_space_opto    = combvec(input_amplitude, input_frequency)';

n_trials            = 10;
n_samples           = 20;
n_subsample         = 300;

acq_function        = 'UCB';
setpoint            = [];

% Subsample the gp_model
n_inputs            = size(input_space_opto,1);
x_subsample         = input_space_opto(randperm(n_inputs,n_subsample),:);         
y_subsample         = gp_model.predict(x_subsample);
y_sp                = linspace(1,.5,100);
d_view              = linspace(-60,50,100);
close all
figure

index = 1;
set(gcf,'color','w');

for c1 = 1:100
    gp_model.initialize_data(x_subsample, -1*abs(y_subsample - y_sp(c1))+y_sp(c1));
    gp_model.lower_bound = [5 5];
    gp_model.upper_bound = [50 42];
    gp_model.update_plot_inputs;
    
    cla
%     xlabel('Amplitude (mW/mm^2)')
%     ylabel('Frequency (Hz)')
%     zlabel('Gamma Power (33-50 Hz)')
    set(gca, 'FontSize', 18)
    gp_model.plot_mean
    colormap(inferno)
    view([d_view(c1), 26])
    
    zlim([0 1])
    xlim([5 50])
    ylim([5 42])
    drawnow
                
%     print(sprintf('in_silico_optimization/results/setpoint_movie_frames/sample_%03d',index), '-dpng', '-r350')
    F               = getframe(gcf);
    [X, Map]        = frame2im(F);
    X_frames{index} = X;
    index           = index+1;
end


%% Configured Optimization
acq_params  = .01;
acq_params  = .04;
n_burn_in   = 5;

rng(6)
for c1 = 1:n_trials
    
    fprintf('%d, ', c1);
    [X_samples_opt_04(c1,:,:), Y_samples_opt_04(c1,:), X_optimal_est_opt_04(c1,:,:), ...
        Y_optimal_est_opt_04(c1,:), Y_ground_truth_opt_04(c1,:)] =  ...
        bayes_opt_on_model(gp_model, input_space_opto, n_samples, n_burn_in, acq_function, acq_params, setpoint);
    
end
fprintf('\n');

%%
% index = 1;
for c2 = 1:n_trials+1
    
    if c2 <= n_trials
        X_samples   = squeeze(X_optimal_est_opt_04(c2,:,:));
        Y_samples   = Y_optimal_est_opt_04(c2,:);
    end
    
    for c1 = 2:n_samples

        X_t     = X_samples(2:c1,:);
        Y_t     = gp_model.predict(X_t);
        cla
        gp_model.plot_mean
        colormap(inferno)

        hold on
               
        for c3 = 1:c2-1
            for c4 = 2:size(X_optimal_est_opt_04,2)
                x1          = X_optimal_est_opt_04(c3,c4-1,1);
                y1          = X_optimal_est_opt_04(c3,c4-1,2);
                x2          = X_optimal_est_opt_04(c3,c4,1);
                y2          = X_optimal_est_opt_04(c3,c4,2);
                
                if x1 == x2
                   x_seg    = x1*ones(1,30);
                   y_seg    = linspace(y1,y2,30);
                else
                    coeffs  = polyfit([x1 x2], [y1 y2],1);
                    x_seg   = linspace(x1,x2,30);
                    y_seg   = polyval(coeffs, x_seg);
                end
                
                z_seg       = gp_model.predict([x_seg; y_seg]')+0.001;
                plot3(x_seg, y_seg, z_seg, '-', 'color', .5*[1 1 1], 'LineWidth',2)
            end
            
            scatter3(X_optimal_est_opt_04(c3,end,1), X_optimal_est_opt_04(c3,end,2), Y_ground_truth_opt_04(c3,end), 60, 'r','filled')

        end
        
        if c2 > n_trials
            drawnow
            F               = getframe(gcf);
            [X, Map]        = frame2im(F);
            X_frames{index} = X;
            index           = index+1;            
            continue
        end
        
        for c3 = 2:size(X_t,1)
            x1      = X_t(c3-1,1);
            y1      = X_t(c3-1,2);
            x2      = X_t(c3,1);
            y2      = X_t(c3,2);
            
            if x1 == x2
               x_seg = x1*ones(1,30);
               y_seg = linspace(y1,y2,30);
            else
                coeffs  = polyfit([x1 x2], [y1 y2],1);
                m       = coeffs(1);
                b       = coeffs(2);

                x_seg   = linspace(x1,x2,30);
                y_seg   = polyval(coeffs, x_seg);
            end
            z_seg   = gp_model.predict([x_seg; y_seg]')+0.001;
            plot3(x_seg, y_seg, z_seg, 'k-', 'MarkerSize', 10, 'MarkerFaceColor', 'k', 'LineWidth',2)
            
                        
        end
        scatter3(X_t(end,1), X_t(end,2), Y_t(end), 60, 'r','filled')
                
        drawnow
        
        F               = getframe(gcf);
        [X, Map]        = frame2im(F);
        X_frames{index} = X;
        index           = index+1; 
    end
end
%%
d_az    = linspace(50, 0);
d_ei    = linspace(26,90);
view([d_view(c1), 26])

for c1 = 1:100
    view([d_az(c1), d_ei(c1)])
    drawnow
    
    F               = getframe(gcf);
    [X, Map]        = frame2im(F);
    X_frames{index} = X;
    index           = index+1; 
end
 
%%
v = VideoWriter('newfile.mp4', 'MPEG-4');
open(v)
for c1 = 1:409
   writeVideo(v,X_frames{c1});
end
close(v)




    