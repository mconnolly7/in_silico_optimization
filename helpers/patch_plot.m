function patch_plot(t, p50, p25, p75, p_color)

tt = [t; flip(t)];
yy = [p25; flip(p75)];

patch(tt, yy, p_color, 'EdgeColor', 'none', 'FaceAlpha', .25);
hold on
plot(t, p50, 'color', p_color, 'LineWidth', 2, 'Marker', 'x');
hold off
end

