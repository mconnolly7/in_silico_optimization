function objective_val = objective_function_noise_2D(next_param, objective_means, objective_noise, scale)

objective_val  = scale * (objective_function_2D(next_param, objective_means , 1) + randn * objective_noise);

end