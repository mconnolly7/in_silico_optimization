function objective_val = objective_function_2D(input, objective_means, scale)

objective_sigma     = eye(2);
objective_val       = mvnpdf(input, objective_means(1,:), objective_sigma) ... 
                        + 0.5 * mvnpdf(input, objective_means(2,:), objective_sigma);
objective_val       = objective_val * scale;
% scatter3(input_space(:,1), input_space(:,2), objective_val)
end
