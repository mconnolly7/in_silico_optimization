function [pos_state, stim_params, optimal_est] = cem_opt_loop(gp_sim, n_trials, n_samples, params)
c1 = 1;
while c1 < n_trials
    try
        [pos_state(:,c1), stim_params(:,:,c1), optimal_est(:,:,c1)] = cem_opt_on_model(gp_sim, n_samples, params);
        c1 = c1 + 1;
    catch
    end
end


end




