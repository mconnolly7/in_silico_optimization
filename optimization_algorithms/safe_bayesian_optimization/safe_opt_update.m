function [x_sample, x_opt_est, y_opt_est, safe_idx, ucb] = safe_opt_update(X_sample, Y_sample, Y_safety, ...
    objective_init, safety_init, beta, eta, alpha, safe_seed, threshold, input_space, USE_HYPERPRIOR, USE_SAFE, PLOT)
  
% Construct objective and safety model
objective_model = objective_init(X_sample, Y_sample);
safety_model    = safety_init(X_sample, Y_safety);

[y_objective_est, ~, ~, fs_objective_est]   = objective_model.predict(input_space);
[y_safety_est, ~, ~, fs_safety_est]         = safety_model.predict(input_space);

if USE_SAFE
    % Calculate uncertainties
    Q_low           = y_safety_est - beta * fs_safety_est;
    Q_high          = y_safety_est + beta * fs_safety_est;

    % Update the safe-set
    safe_idx        = Q_high < threshold;

end

if all(safe_idx == 0)
    [~, min_safe_idx]       = min(Q_low);
    safe_idx(min_safe_idx)  = 1;
end

safe_input_space        = input_space(safe_idx,:);
safe_objective_est      = y_objective_est(safe_idx);
safe_objective_fs       = fs_objective_est(safe_idx);
t                       = size(X_sample,1);

[ucb, x_sample]         = upper_confidence_bound(safe_input_space, safe_objective_est, safe_objective_fs, t, eta);
[y_opt_est, x_opt_idx]  = max(safe_objective_est);    
x_opt_est               = safe_input_space(x_opt_idx,:);

if PLOT
    subplot(2,3,1); hold off
    objective_model.plot_mean
    hold on
    objective_model.plot_data
    
    subplot(2,3,4); hold off
    safety_model.plot_mean
    hold on
    safety_model.plot_data

    subplot(2,3,2); hold off
    scatter3(safe_input_space(:,1), safe_input_space(:,2), ucb)
    xlabel('Contact')
    ylabel('Amplitude (mA)');
    legend('Estimated Safe Parameters')
    colormap('jet');
    view(0,90);
    xlim([.5 10.5])
    ylim([0 5])
end
end


