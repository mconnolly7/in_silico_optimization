function optimization_results = multi_objective_on_model(optimization_config, algorithm_config, PLOT)

  

search_strategy     = algorithm_config.search_strategy;
search_parameters   = algorithm_config.search_parameters;
model_function      = algorithm_config.model_function;
hyperprior_data     = algorithm_config.hyperprior_data;
hyperprior_param    = algorithm_config.hyperprior_param;

objective_1         = optimization_config.objective_1;
objective_2         = optimization_config.objective_2;
objective_1_dir     = optimization_config.objective_1_dir;
objective_2_dir     = optimization_config.objective_2_dir;
input_space         = optimization_config.input_space;
n_samples           = optimization_config.n_samples;

ref_point           = [0 20];
%
% Generate burn-in samples
%        
switch search_strategy
    case 'grid'
        n_grid              = search_parameters(1);
        n_reps              = floor(n_samples/n_grid);
        
        grid_idx            = randperm(size(input_space,1));
        grid_input          = input_space(grid_idx(1:n_grid),:);
    
        X_sample            = repmat(grid_input, n_reps,1);

    case 'random'
        rand_idx            = randi(size(input_space,1),n_samples);
        X_sample            = input_space(rand_idx(1:n_samples),:);  
    
    case 'evolution'
        generation_size     = search_parameters(1);
        gen_1_idx           = randi(size(input_space,1),generation_size,1);
        X_sample            = input_space(gen_1_idx,:);
        
    case 'ehvi'
        n_burn_in           = search_parameters(1);
        ehvi_param          = search_parameters(2);
        burn_in_idx         = randi(size(input_space,1),n_burn_in,1);
        X_sample            = input_space(burn_in_idx,:);
end


if PLOT
    close all
    fig_1 = figure( 'Position',  [600, 100, 1000, 900]);
end

% Generate ground truth pareto idx for real-time error plotting
Y1_est              = objective_1.predict(input_space);
Y2_est              = objective_2.predict(input_space);

% Estimate Pareto front
costs_gt                        = [Y1_est Y2_est];
[pareto_idx_gt, dom_order_gt] 	= get_pareto_2(objective_1_dir,objective_2_dir, costs_gt);
pareto_set_gt                   = costs_gt(pareto_idx_gt,:);


for c1 = 1:n_samples

    fprintf('%d, ', c1)
    if c1 > 2 % Calculate the current estimate of the Pareto set

        
        if ~isempty(model_function{1}) 
            % If there is a function for modeling, 
            % use it to estimate the Pareto front
            
            % Construct GP model of objectives
            objective_1_est(c1)                 = model_function{1}(X_sample(1:c1-1,:), y1_sample, hyperprior_data{1}, hyperprior_param);
            objective_2_est(c1)                 = model_function{2}(X_sample(1:c1-1,:), y2_sample, hyperprior_data{2}, hyperprior_param);    
            
            % Evaluate objective on set of candidate points
            Y1_est                              = objective_1_est(c1).predict(input_space);
            Y2_est                              = objective_2_est(c1).predict(input_space);
            
            % Estimate Pareto front
            costs_est                           = [Y1_est Y2_est];
            [pareto_sample, dom_order_sample]   = get_pareto_2(objective_1_dir,objective_2_dir, costs_est);

            pareto_idx(:,c1-1)                  = pareto_sample;  
            dom_order_est(:,c1-1)               = dom_order_sample;
            pareto_set                          = costs_est(pareto_idx(:,c1-1),:);
            
            Y_est(:,:,c1-1)                     = costs_est;

        else % calculate Pareto set using the noisy outputs
            
            objective_1_est(c1)                 = NaN;
            objective_2_est(c1)                 = NaN;
            
            % Use Y_sample to get Pareto set
            [pareto_sample, dom_order_sample]   = get_pareto_2(objective_1_dir,objective_2_dir, Y_sample);
            
            % Without a model, only inputs actually sampled can be part of
            % the Pareto set. 
         	pareto_idx(:,c1-1)                 	= zeros(size(input_space,1),1);
            dom_order_est(:,c1-1)              	= max(dom_order_sample) + ones(size(input_space,1),1);
            
            % Calcuate Pareto set and dominance order for sampled inputs
            for c2 = 1:c1-1
                input_idx                       = all(input_space == X_sample(c2,:),2);
                pareto_idx(input_idx,c1-1)      = pareto_sample(c2);  
                dom_order_est(input_idx,c1-1)   = dom_order_sample(c2);
            end
            
            pareto_set                          = Y_sample(pareto_sample,:);
            Y_est(:,:,c1-1)                     = NaN;

        end
        
        % Real-time metrics
        pareto_distance(c1-1,1) = mean(min(pdist2(pareto_set, pareto_set_gt),[],2));
        pareto_distance(c1-1,2) = mean(min(pdist2(pareto_set, pareto_set_gt),[],1));
        
        auc(c1-1,1)             = pareto_roc_2(dom_order_est(:,c1-1), dom_order_gt);

    end
    
    % Calculate the next sample
    
    switch search_strategy

        case 'evolution' % Update after each generation is completed
            if mod(c1, generation_size) == 0

                elite_threshold = 0;

                while sum(dom_order_sample <= elite_threshold) < generation_size
                    elite_threshold = elite_threshold+1;
                end
                
                elite_idx       = find(dom_order_sample <= elite_threshold);
                rand_idx        = randperm(size(elite_idx,1));
                
                elite_idx_rand  = elite_idx(rand_idx(1:generation_size));
                elite_samples   = input_space(elite_idx_rand,:);
                
                X_sample(c1+1:c1+generation_size,:) = elite_samples;
            end

        case 'ehvi' % Update after every sample
            if c1 > n_burn_in
                input_sample = expected_hypervolume_improvement(objective_1_est(c1), objective_2_est(c1),...
                input_space, ref_point, ehvi_param, model_function, hyperprior_data, hyperprior_param);

                X_sample(c1,:)     = input_sample;
            end
    end

    % Evaluate new sample
    y1_sample(c1,:)    = objective_1.sample(X_sample(c1,:));
    y2_sample(c1,:)    = objective_2.sample(X_sample(c1,:));
    Y_sample           = [y1_sample y2_sample];
    

    %%%%%%%
    % Plotting
    %%%%%%%
    if c1 > 2 && PLOT
        allAxes = findall(0,'type','axes');

        for a1 = 2:size(allAxes,1)
            cla(allAxes(a1))
        end
        
        set(0, 'CurrentFigure', fig_1)
        subplot(2,3,[3 6])
        hold off
        
        max_dom_order = 5;
        dom_color_map = colormap(hot(max_dom_order*3));
        dom_color_map = flip(dom_color_map(1:max_dom_order+1,:));
        
        plot_idx = dom_order_est(:,c1-1) > max_dom_order;
        scatter(Y1_est(plot_idx),Y2_est(plot_idx),50, [.5 .5 .5])
        
        hold on
        for c2 = 0:max_dom_order
            plot_idx = dom_order_est(:,c1-1) == c2;
            scatter(Y1_est(plot_idx),Y2_est(plot_idx),100,dom_color_map(c2+1,:),'filled', 'MarkerFaceAlpha',.5) 
        end
        
        scatter(costs_gt(pareto_idx_gt,1), costs_gt(pareto_idx_gt,2), 100, 'k')

        if isobject(objective_1_est(c1)) && size(input_space,2) == 3
            h1 = subplot(2,3,2); cla(h1)       
            plot_bipolar(objective_1_est(c1))

            h2 = subplot(2,3,5); cla(h2)
            plot_bipolar(objective_2_est(c1))
        end
        
        h3 = subplot(2,3,1); cla(h3)
        plot(pareto_distance)    
        xlim([1,n_samples])
              
        h4 = subplot(2,3,4); cla(h4)
        plot(auc)
        xlim([1,n_samples])
        ylim([0,1])
        drawnow
    end
end

optimization_results.X_sample           = X_sample;
optimization_results.Y_sample           = Y_sample;
optimization_results.pareto_idx         = pareto_idx;
optimization_results.dom_order_est      = dom_order_est;
optimization_results.Y_est              = Y_est;
optimization_results.objective_est      = [objective_1_est; objective_2_est]';
end



