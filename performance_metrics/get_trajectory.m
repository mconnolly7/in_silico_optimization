function get_trajectory
data_path       = '/Users/mconn24/Box Sync/papers/2019_12_31_paper_safe_optimization/memory_data/ARN082_t.xlsx';

% data_path       = 'memory_project/data/ARN082_t.xlsx';
data_table      = readtable(data_path);


safe_samples    = [0 .5 1];
lower_bound     = 0;
upper_bound     = 4;

USE_HYPERPRIOR  = 1;
USE_SAFE_OPT    = 1;
PLOT            = 0;

BETA            = -1;
ETA             = .1;
alpha           = 1;

X_v             = data_table.voltage_new;
Y_ds            = data_table.DS;

nan_idx         = isnan(Y_ds);

X_v             = X_v(~nan_idx);
Y_ds            = Y_ds(~nan_idx);

v_sample        = safe_samples;
for c1 = 3:size(X_v,1)
    [v_new, v_opt, area_opt, S, ucb] = safe_opt_update(X_v(1:c1), Y_ds(1:c1), BETA, ETA, alpha, ...
        safe_samples, 0, lower_bound, upper_bound, USE_HYPERPRIOR, USE_SAFE_OPT, PLOT);

    v_trajectory(c1)    = v_opt;
    v_sample(c1+1)    = v_new;
end

close all
hold on
plot(v_trajectory, 'k-', 'LineWidth', 2)
plot(v_sample, 'k--', 'LineWidth', 1)
xlabel('Sample')
ylabel('Estimated Optimal V')
set(gca, 'FontSize', 16);
legend({'Estimated Optimal V', 'Sampled V'})
title('ARN081')
end

