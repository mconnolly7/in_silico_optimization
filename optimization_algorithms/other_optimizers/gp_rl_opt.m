function [pre_state, pos_state, stim_params] = gp_rl(gp_sim, n_trials, params)

for c1 = 1:n_trials
    [pre_state(:,c1), pos_state(:,c1), stim_params(:,:,c1)] = dynamic_opt(gp_sim, params);
end

end


function [pre_state, pos_state, stim_params] = dynamic_opt(gp_sim, params)

gp_sim.set_initial_state()

p_duration  =  polyfit([0 1], [15 120],1);
p_amplitude =  polyfit([0 1], [1 4],1);

n_burn_in   = 5;
n_learning  = 95;
n_ghost     = 100;

for c1 = 1:n_burn_in    
    stim_params(c1,1)   = polyval(p_duration, rand);
    stim_params(c1,2)   = polyval(p_amplitude, rand);
    
    pre_state(c1,1)     = gp_sim.current_state();
    pos_state(c1,1)     = gp_sim.sample(stim_params(c1,:));
end

% Learning
for c1 = n_burn_in+1:n_burn_in + n_learning
    policy_model = simulation_gp();

    x = [pre_state stim_params];
    y = pos_state;
    
    policy_model.initialize_data(x,y,[15 1], [120 4])
    policy_model.gp_model.minimize(10)
    
    pre_state(c1,1)     = gp_sim.current_state();
    switch params(1)
        case 1
            stim_params(c1,:)   = policy_model.state_aquisition(pre_state(c1,1), 'PI', params(2));
        case 2
            stim_params(c1,:)   = policy_model.state_aquisition(pre_state(c1,1), 'EI', params(2));
        case 3
            stim_params(c1,:)   = policy_model.state_aquisition(pre_state(c1,1), 'UCB', params(2));
    end
    pos_state(c1,1)     = gp_sim.sample(stim_params(c1,:));
    
end

% Ghost
gp_sim.set_initial_state()
for c1 = n_burn_in + n_learning + 1:n_burn_in + n_learning + n_ghost
    
    pre_state(c1,1)     = gp_sim.current_state();
    stim_params(c1,:)   = policy_model.state_extrema(pre_state(c1,1));

    pos_state(c1,1)     = gp_sim.sample(stim_params(c1,:));
end

end