input_space_1   = -2:1:2;
input_space_2   = -2:1:2;
input_space     = combvec(input_space_1, input_space_2)';

y = mvnpdf(input_space, [ 0 0], [1 0; 0 1])

scatter3(input_space(:,1),input_space(:,2), y) 