function [X_samples, Y_samples, X_optimal_est, Y_optimal_est, Y_ground_truth] = bayes_opt_loop(...
    objective_model, input_space, n_trials, n_samples, n_burn_in, acquisition_function, acq_params, measurement_function)

for c1 = 1:n_trials
    fprintf('\tTrial %d\n', c1);

    [X_samples(:,:,c1), Y_samples(:,c1) , X_optimal_est(:,:,c1),  Y_optimal_est(:,c1), Y_ground_truth(:,c1)] ...
        =  bayes_opt_on_model(objective_model, input_space, n_samples, n_burn_in, acquisition_function, acq_params, measurement_function);
end

end


