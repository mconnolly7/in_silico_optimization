
LB      = [0 0 0 0 0];
UB      = [1000 1000 5 5 20];

opt     = optimoptions('ga','Display','iter','PlotFcn',@gaplotbestf);
nvars   = 5;

[x,fval,exitflag,output,population,scores] =            ...
    ga(@SPSA_cost_function, nvars, [], [], [], [], LB, UB, [], opt);

%147.8388    7.0646    1.4003    1.2326   -1.7267