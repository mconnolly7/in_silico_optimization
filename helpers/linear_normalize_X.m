function [X_norm, m, b] = linear_normalize_X(X, lower, upper)

for c1 = 1:size(X,2)
    p               = polyfit([lower(c1) upper(c1)], [0 1], 1);
    m(c1)           = p(1);
    b(c1)           = p(2);
    X_norm(:,c1)    = polyval(p, X(:,c1));
end
end

