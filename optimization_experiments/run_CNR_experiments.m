clear 

%% 1D
gamma_path              = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_1D/R1_gamma_1D.mat';
load(gamma_path)

objective_model{1}      = sphere_1D_model;  
input_space{1}          = linspace(-1,1, 1000)';


objective_model{2}      = gp_model;
input_space{2}          = linspace(5,42, 1000)';

objective_model{3}      = rastrigin_1D_model;
input_space{3}          = linspace(-5,5, 1000)';

objective_model{4}      = easom_1D_model;
input_space{4}          = linspace(0,1, 1000)';

dimension = 1;

%% 2D
gamma_path              = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_2D/R1_gamma.mat';
load(gamma_path)


objective_model{1}      = sphere_2D_model;
t1                      = linspace(-1, 1, 33);
t2                      = linspace(-1, 1, 33);
input_space{1}          = combvec(t1,t2)';

objective_model{2}      = gp_model;
t1                      = linspace(10, 50, 33);
t2                      = linspace(5, 42, 33);
input_space{2}          = combvec(t1,t2)';

objective_model{3}      = rastrigin_2D_model;
t1                      = linspace(-5,5, 33);
t2                      = linspace(-5,5, 33);
input_space{3}          = combvec(t1,t2)';

objective_model{4}      = easom_2D_model;
t1                      = linspace(0, 1, 33);
t2                      = linspace(0, 1, 33);
input_space{4}          = combvec(t1,t2)';

dimension = 2;

%% 3D
gamma_path              = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_3D/R1_gamma_3D.mat';
load(gamma_path)


objective_model{1}      = sphere_3D_model;
t1                      = linspace(-1, 1, 33);
t2                      = linspace(-1, 1, 33);
t3                      = linspace(-1, 1, 33);
input_space{1}          = combvec(t1,t2, t3)';

objective_model{2}      = gp_model;
t1                      = linspace(10, 50, 33);
t2                      = linspace(5, 42, 33);
t3                      = linspace(2, 10, 33);
input_space{2}          = combvec(t1,t2, t3)';

objective_model{3}      = rastrigin_3D_model;
t1                      = linspace(-5,5, 33);
t2                      = linspace(-5,5, 33);
t3                      = linspace(-5,5, 33);
input_space{3}          = combvec(t1, t2, t3)';

objective_model{4}      = easom_3D_model;
t1                      = linspace(0, 1, 33);
t2                      = linspace(0, 1, 33);
t3                      = linspace(0, 1, 33);
input_space{4}          = combvec(t1, t2, t3)';

dimension = 3;

%% 
n_trials                = 30;
n_samples               = 100;
CNR_list                = [5 4 3 2 1.5 1 .5 .25 .1];
% CNR_list                = [5 2 1 .1];

for c1 = 3;size(objective_model,2)
    
    [opt_data(:,c1), opt_results(c1)] = CNR_experiment(objective_model{c1}, input_space{c1}, n_trials, n_samples, CNR_list);
end


%%
n_trials                = 30;
CNR_list                = [5 4 3 2 1.5 1 .5 .25 .1];

dimension = 2
switch dimension
    case 1
        load('in_silico_optimization/results/benchmark_results/results_1D.mat')
        titles_list       = {'Sphere 1D', 'R1-frequency', 'Rastrigin 1D', 'Easom 1D'};

    case 2
        load('in_silico_optimization/results/benchmark_results/results_2D.mat')
        titles_list       = {'Sphere 2D', 'R1-freq, amp', 'Rastrigin 2D', 'Easom 2D'};

    case 3
%         load('in_silico_optimization/results/benchmark_results/results_3D.mat')
        titles_list      = {'Sphere 3D', 'R1-freq, amp, pw', 'Rastrigin 3D', 'Easom 3D'};

end
close all
f               = figure( 'Position',  [190, 100, 800, 1000]);

x_grid          = [.1 .55];
y_grid          = [.79 .52 .30 .1]-.03;

width           = [.4 .85];
height          = [.18 .3] ;

ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(2)  y_grid(1) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(1)  y_grid(2) width(1) height(1)]);
ax(4)        	= axes('Position', [x_grid(2)  y_grid(2) width(1) height(1)]);
ax(5)        	= axes('Position', [x_grid(1)  y_grid(3) width(2) height(1)]);
ax(6)        	= axes('Position', [x_grid(1)  y_grid(4) width(2) height(1)]);

for c1 = 1:4
    set(f, 'currentaxes', ax(c1));

    objective_model = opt_data(1,c1).objective_model;
    x               = opt_data(1,c1).input_space;
    y               = objective_model.predict(x);

    switch dimension
        case 1
            
            plot(x,y, 'LineWidth', 3, 'color', 'k')
            title(titles_list{c1})
            set(gca,'FontSize', 14)
            xlim([min(x), max(x)])
        case 2
    
            t1 = x(:,1);
            t2 = x(:,2);
            
            n_t1 = size(unique(t1),1);
            
            x1r = reshape(t1,n_t1,[]);
            x2r = reshape(t2,n_t1,[]);
            yr  = reshape(y,n_t1,[]);
            surf(x1r,x2r,yr)

            title(titles_list{c1})
            set(gca, 'FontSize', 14)
            
    end

    
    box off
end

offset = [-.025 -.05 .05 .025];

for c1 = 1:size(opt_results,2)
    
    m           = mean(opt_results(c1).Y_error_rel);
    s           = std(opt_results(c1).Y_error_rel);
    se          = s/sqrt(n_trials);
    
    set(f, 'currentaxes', ax(5));
    hold on
    x_offset = CNR_list+offset(c1);
    h(c1) = plot(x_offset, m, 'color', dark2(c1), 'LineWidth', 3);
    plot([x_offset; x_offset], [m-s; m+s], 'color', dark2(c1), 'LineWidth', 3 )
    
    set(f, 'currentaxes', ax(6));
    hold on
    plot(x_offset, opt_results(c1).conv_rate_m, 'color', dark2(c1), 'LineWidth', 3)
    plot([x_offset; x_offset], opt_results(c1).conv_rate_ci, 'color', dark2(c1), 'LineWidth', 3)
    
    set(gca, 'FontSize', 14)

end

set(f, 'currentaxes', ax(5));

x_ticks = sort(CNR_list);
x_ticks(2) = [];
xticks(x_ticks)
xticklabels([])
ylabel('Y_{error}')
set(gca, 'FontSize', 14)
xlim([0 5.2])
l =legend(h, titles_list, 'box', 'off', 'position',[0.7837 0.3990 0.1450 0.0655])
box off

set(f, 'currentaxes', ax(6));


xticks(x_ticks)
xlabel('CNR')
ylabel('Rate Coefficient')
set(gca, 'FontSize', 14)
xlim([0 5.2])
box off
y_coords        = [.87  .37 ];
x_coords        = [.02];
sublabel_size   = 24;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(2) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
print(sprintf('stress_test_%d',dimension), '-dpng', '-r500')

%%
t = input_space{3};
y = objective_model{3}.predict(input_space{3});


t1              = t(:,1);
t2              = t(:,2);
t3              = t(:,3);

subplot(4,1,1)
plot(y)

subplot(4,1,2)
plot(t1)

subplot(4,1,3)
plot(t2)

subplot(4,1,4)
plot(t3)






