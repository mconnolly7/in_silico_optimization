function [X_opt, Y_opt, X_sample, Y_sample] =  safe_opt_loop(objective_model,...
    beta, eta, alpha, PLOT, lower_bound, upper_bound, safe_seed, threshold, USE_HYPERPRIOR, USE_SAFE_OPT, n_trials)

for trial = 1:n_trials
    fprintf('\tTrial %d\n', trial);
    

    [X_opt(:,trial), Y_opt(:,trial), X_sample(:,trial), Y_sample(:,trial)] = safe_opt_on_model(objective_model, ...
    beta, eta, alpha, PLOT, lower_bound, upper_bound, safe_seed, threshold, USE_HYPERPRIOR, USE_SAFE_OPT);

  
end
end
