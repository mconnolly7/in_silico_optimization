% function objective_fig()
rng(1);
close all;

[x, y_meas, gp_model] = objective_data();

hold on

xlim([-2.5 2.5])
ylim([-0.5 2])
xlabel('Input')
ylabel('Output')

set(gca,'FontSize', 16);
x_1     = 0;
y_1     = gp_model.sample(x_1);


X       = x_1;
Y       = y_1;

scatter(X, Y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 0 0],...
    'LineWidth',2)


%%
rng(1);
figure; hold on
% gp_model.plot_standard_deviation();
% gp_model.plot_mean();

x_1     = 0;
y_1     = gp_model.sample(x_1);

x_2     = 0;
y_2     = gp_model.sample(x_2);

X       = [x_1; x_2];
Y       = [y_1; y_2];


scatter(X, Y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 0 0],...
    'LineWidth',2)


xlim([-2.5 2.5])
ylim([-0.5 2])
xlabel('Input')
ylabel('Output')

set(gca,'FontSize', 16);

%%
close all;
rng(1);
figure; hold on

n_samples   = 20;

X           = [-2*ones(n_samples,1); 0*ones(n_samples,1); 2*ones(n_samples,1)];
Y           = gp_model.sample(X);

scatter(X, Y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 0 0],...
    'LineWidth',2)

xlim([-2.5 2.5])
ylim([-0.5 2])
xlabel('Input')
ylabel('Output')

set(gca,'FontSize', 16);

figure; hold on

h           = boxplot(Y,X, 'notch', 'on', 'Positions', unique(X));

set(h, 'color', 0*ones(1,3));
set(h, 'LineWidth', 2);

scatter(X, Y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 0 0],...
    'LineWidth',2)

xlim([-2.5 2.5])
ylim([-0.5 2])
xlabel('Input')
set(gca,'FontSize', 16);

ylabel('Output')

%%
close all;
rng(1);
figure; hold on

n_samples   = 20;

X           = uniform_range(-2.5, 2.5, 60);
Y           = gp_model.sample(X);

scatter(X, Y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 0 0],...
    'LineWidth',2)

xlim([-2.5 2.5])
ylim([-0.5 2])
xlabel('Input')
ylabel('Output')

set(gca,'FontSize', 16);

%%
close all;
rng(1);
figure; hold on

n_samples   = 20;

X           = uniform_range(-2.5, 2.5, 60);
Y           = gp_model.sample(X);

gp_model.plot_standard_deviation();
gp_model.plot_mean();

scatter(X, Y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 0 0],...
    'LineWidth',2)

xlim([-2.5 2.5])
ylim([-0.5 2])
xlabel('Input')
ylabel('Output')

set(gca,'FontSize', 16);

%%
close all;
rng(1);
figure; hold on

n_samples   = 20;

X           = uniform_range(-2.5, 2.5, 60);
Y           = gp_model.sample(X);

gp_model.plot_standard_deviation();
gp_model.plot_mean();

scatter(X, Y, 100,...
    'MarkerEdgeColor',[0 0 0],...
    'MarkerFaceColor',[.7 0 0],...
    'LineWidth',2)

xlim([-2.5 2.5])
ylim([-0.5 2])
xlabel('Input')
ylabel('Output')

set(gca,'FontSize', 16);