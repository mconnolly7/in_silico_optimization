close all
f = figure('Position', [0 0, 2100, 900]);

x_grid          = [.02 .34 .66];
y_grid          = [.67  .36  .05]-.01;

width           = .3;
height          = .3;

ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(2)  y_grid(1) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(3)  y_grid(1) width(1) height(1)]);
ax(4)        	= axes('Position', [x_grid(1)  y_grid(2) width(1) height(1)]);
ax(5)        	= axes('Position', [x_grid(2)  y_grid(2) width(1) height(1)]);
ax(6)        	= axes('Position', [x_grid(3)  y_grid(2) width(1) height(1)]);
ax(7)        	= axes('Position', [x_grid(1)  y_grid(3) width(1) height(1)]);
ax(8)        	= axes('Position', [x_grid(2)  y_grid(3) width(1) height(1)]);
ax(9)        	= axes('Position', [x_grid(3)  y_grid(3) width(1) height(1)]);

spsa_fig
cem_fig
bayes_fig

print( 'figure_3','-depsc')
