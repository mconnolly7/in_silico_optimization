clear; clc;
% data_dir        = 'optogenetic_optimization_project/data/in_silico_data/processed_data/park_2019/iso-awake_static_2019_02_06/';
data_dir        = 'optogenetic_optimization_project/data/in_silico_data/processed_data/park_2019/iso-awake_sequence/';

psd_data_iso    = [data_dir 'PSD_4_second_sequence_amp-freq_ISO_2019_02_02.mat'];
psd_data_awake  = [data_dir 'PSD_4_second_sequence_amp-freq_2019_01_24.mat'];

% psd_data_iso    = [data_dir 'Mark_4sec_CA3PSD_ISO_freqamp_020619'];
% psd_data_awake  = [data_dir 'Mark_4sec_CA3PSD_awake_freqamp_020619.mat'];

input_amplitude     = 0:50;
input_frequency     = 5:35;
input_space         = combvec(input_amplitude, input_frequency)';

delta           = 1:4;
theta           = 4:7;
alpha           = 8:15;
beta            = 16:31;
gamma           = 32:50;
high_gamma      = 50:70;

[X0_awk, X1_awk, U1_awk]    = build_dataset(gamma, psd_data_awake);
[X0_iso, X1_iso, U1_iso]    = build_dataset(gamma, psd_data_iso);

X0              = [X0_awk; X0_iso];
X1              = [X1_awk; X1_iso];
U1              = [U1_awk; U1_iso];

out_idx         = detect_outlier([X0 X1 U1],5);

X0              = X0(~out_idx,:);
X1              = X1(~out_idx,:);
U1              = U1(~out_idx,:);
U1              = [U1(:,2), U1(:,1)];

state_list      = linspace(min(X0), max(X0), 100);
% state_list      = [state_list flip(state_list)];

dynamics_model  = simulation_gp();
dynamics_model.initialize_data_nonstationary(X0, U1, X1, min(U1), max(U1), state_list);


% dynamics_model.gp_model.minimize(10)

for rng_seed = 1:10
    n_samples   = 100;
    params      = [3 4];
    % rng_seed    = 2;
    rng(rng_seed)
    
    for trial = 1:5
        fprintf('Trial: %d\n', trial)
        
        % Train BaO
        %     fprintf('\tTrain BaO\n')
        %
        %     dynamics_model.state_idx = 1;
        %     [pos_state_bao, stim_params_bao, optimal_hist_bao] =  bayes_opt_on_model(dynamics_model, n_samples, params);
        %     optimal_bao(trial,:) = optimal_hist_bao(end,2:3);
        %
        %
        %     %% Test BaO
        %     fprintf('\tTest BaO\n')
        %
        %     dynamics_model.state_idx = 1;
        %     for c1 = 1:100
        %         bao_samples(trial,c1) = dynamics_model.sample(optimal_bao(trial,:));
        %     end
        
        
        % Train SDBO
        fprintf('\tTrain SDBO\n')
        dynamics_model.state_idx = 1;
        [pos_state, stim_params, optimal_sdbo] =  sdbo_opt_on_model(dynamics_model, 100, params);
        
        trial_policy(trial) = optimal_sdbo(end);
        
        %     %% Test SDBO
        %     fprintf('\tTest SDBO\n')
        %
        %
        %
        %     dynamics_model.state_idx = 1;
        %     for c1 = 1:100
        %         current_state(c1)       = dynamics_model.current_state;
        %         opt_stim(c1,:)          = policy(trial).state_extrema(current_state(c1));
        %         sdbo_samples(trial,c1)  = dynamics_model.sample(opt_stim(c1,:));
        %     end
    end
    
    
    %%
    state_cross_section = gp_object;
    close all;
    clear opt_stim
    
    f = figure('Position', [-961 181 808 604]);
    
    index = 1;
    set(gcf,'color','w');
    state_list_2 = [state_list flip(state_list)];
    
    input_amplitude     = 0:5:50;
    input_frequency     = 5:5:35;
    input_space         = combvec(input_amplitude, input_frequency)';
    
    
    for c1 = 1:100
        X_pre = repmat(state_list_2(c1), size(input_space,1),1);
        Y     = dynamics_model.gp_model.predict([X_pre input_space]);
        
        state_cross_section.initialize_data(input_space, Y);
        state_cross_section.plot_mean
        
        for c2 = 1:5
            policy = trial_policy(c2);
            
            opt_stim(c1,c2,:)  = policy.state_extrema(state_list_2(c1));
            
            if c1 > 1
                
                y_opt               = state_cross_section.predict(squeeze(opt_stim(:,c2,:)));
                
                hold on
                for c3 = 2:size(opt_stim,1)
                    x1          = opt_stim(c3-1,c2,1);
                    y1          = opt_stim(c3-1,c2,2);
                    x2          = opt_stim(c3,c2,1);
                    y2          = opt_stim(c3,c2,2);
                    
                    if x1 == x2
                        x_seg    = x1*ones(1,10);
                        y_seg    = linspace(y1,y2,10);
                    else
                        coeffs  = polyfit([x1 x2], [y1 y2],1);
                        x_seg   = linspace(x1,x2,10);
                        y_seg   = polyval(coeffs, x_seg);
                    end
                    
                    z_seg       = state_cross_section.predict([x_seg; y_seg]')+1e-12;
                    plot3(x_seg, y_seg, z_seg, '-', 'color', .5*[1 1 1], 'LineWidth',2)
                end
                
                %             plot3(opt_stim(:,c2,1), opt_stim(:,c2,2), y_opt, 'k-', 'LineWidth', 2, 'MarkerSize', 12)
                
                scatter3(opt_stim(end,c2,1), opt_stim(end,c2,2), y_opt(end), 60, 'r' ,'filled')
            end
            
        end
        
        xlim([0 50])
        ylim([5 35])
        zlim([0 max(X1)*1.1])
        xlabel('Amplitude (mW/mm^2)')
        ylabel('Frequency (Hz)')
        zlabel('Gamma Power (33-50 Hz)')
        set(gca,'FontSize',18)
        colormap(inferno)
        drawnow
        
        F               = getframe(gcf);
        [X, Map]        = frame2im(F);
        X_frames{index} = X;
        index           = index+1
        
        cla
    end
    
    %%
    v = VideoWriter(sprintf('non_stationary_%d.mp4',rng_seed), 'MPEG-4');
    open(v)
    for c1 = 1:100
        writeVideo(v,X_frames{c1});
    end
    close(v)
    
end