function [W_out_ga, f_val, exit_flag, output, population, scores, rc_struct] ...
    = train_ESN_opt(n_parameters, n_measurements, n_trials, spectral_radius, reservior_size)
close all
n_samples                   = 100;
    
% generate the ESN reservoir
in_size                     = n_parameters + n_measurements; 
out_size                    = n_parameters;

% rand('seed', 42 );

W_in                        = (rand(reservior_size,1+in_size)-0.5) .* 1;
W                           = rand(reservior_size,reservior_size)-0.5;

% normalizing and setting spectral radius
opt.disp                    = 0;
rhoW                        = abs(eigs(W,1,'LM',opt));
W                           = W .* (spectral_radius / rhoW);

x                           = rand(reservior_size,1)*2 - 1;
W_out                       = rand(2,1+in_size+reservior_size);

rc_struct.out_size          = out_size;
rc_struct.n_samples         = n_samples;

rc_struct.W_in              = W_in;
rc_struct.W                 = W;
rc_struct.W_out             = W_out;
rc_struct.x                 = x;
rc_struct.n_params          = 5;
n_vars                      = 1 + (in_size + reservior_size + 1) * out_size + reservior_size;


lower_bound                 = [0 ones(1,n_vars-1) * -10];
upper_bound                 = [1 ones(1,n_vars-1) * 10];

options                     = optimoptions('ga','PlotFcn', @gaplotbestf);
% options.MaxGenerations      = 500;
options.UseParallel         = true;
PLOT                        = 0;
fun                         = @(W_test)eval_model_ND(W_test, rc_struct, n_trials, PLOT);

[W_out_ga, f_val, exit_flag, output, population, scores]  ...
    = ga(fun,n_vars,[],[],[],[],lower_bound,upper_bound,[],options);

rc_struct.W_out_ga          = W_out_ga;

end