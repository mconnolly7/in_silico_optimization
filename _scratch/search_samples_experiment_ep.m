clear

%% Load EP model
input_cathode   = 0:1:3;
input_anode     = 0:1:3;
input_amp       = 1:.05:5;
input_pulse     = 20:1:120; 
input_space   	= combvec(input_cathode, input_anode, input_amp, input_pulse)';

n_samples       = 90;
n_trials        = 15;
acq_function    = 'UCB';
setpoint        = [];

%%
gp_model       = model_ep_emg_objective_4D(0);




%% Random search
acq_params  = .0;
n_burn_in   = n_samples;

rng(0)

for c1 = 1:n_trials
    
    fprintf('%d, ', c1);
    [X_samples_ep_rand(c1,:,:), Y_samples_ep_rand(c1,:), X_optimal_est_ep_rand(c1,:,:), ...
        Y_optimal_est_ep_rand(c1,:), Y_ground_truth_ep_rand(c1,:)] =  ...
        bayes_opt_on_model(gp_model, input_space, n_samples, n_burn_in, acq_function, acq_params, setpoint);
    
end
fprintf('\n');


 
%% Configured Optimization
acq_params  = .4;
n_burn_in   = 15;

rng(0)

for c1 = 1:n_trials
    
    fprintf('%d, ', c1);
    [X_samples_opt_ep_04(c1,:,:), Y_samples_opt_ep_04(c1,:), X_optimal_est_opt_ep_04(c1,:,:), ...
        Y_optimal_est_opt_ep_04(c1,:), Y_ground_truth_opt_ep_04(c1,:)] =  ...
        bayes_opt_on_model(gp_model, input_space, n_samples, n_burn_in, acq_function, acq_params, setpoint);
    
end
fprintf('\n');


%% Configured Optimization 2
acq_params  = 0;
n_burn_in   = n_trials;

rng(0)

for c1 = 1:15
    
    fprintf('%d, ', c1);
    [X_samples_opt_ep_0(c1,:,:), Y_samples_opt_ep_0(c1,:), X_optimal_est_opt_ep_0(c1,:,:), ...
        Y_optimal_est_opt_ep_0(c1,:), Y_ground_truth_opt_ep_0(c1,:)] =  ...
        bayes_opt_on_model(gp_model, input_space, n_samples, n_burn_in, acq_function, acq_params, setpoint);
    
end
fprintf('\n');
 
 
%%
cla
d = [166,206,227
31,120,180
178,223,138]/255;

hold on
m   = mean(Y_ground_truth_ep_rand(:,2:end));
s   = std(Y_ground_truth_ep_rand(:,2:end));
se  = s / sqrt(n_trials);
ci  = se*2.131;

t   = 2:n_samples;
tt  = [t flip(t)];
cc  = [m - ci flip(m + ci)];

plot(t, m, 'color', d(3,:), 'LineWidth', 2)
patch(tt,cc, d(3,:), 'FaceAlpha', .4, 'EdgeColor', d(3,:));

%
m   = mean(Y_ground_truth_opt_ep_04(:,2:end));
s   = std(Y_ground_truth_opt_ep_04(:,2:end));
se  = s / sqrt(n_trials);
ci  = se*2.131;

t   = 2:n_samples;
tt  = [t flip(t)];
cc  = [m - ci flip(m + ci)];

plot(t, m, 'color', d(2,:), 'LineWidth', 2)
patch(tt,cc, d(2,:), 'FaceAlpha', .4, 'EdgeColor', d(2,:))

m   = mean(Y_ground_truth_opt_ep_0(:,2:end));
s   = std(Y_ground_truth_opt_ep_0(:,2:end));
se  = s / sqrt(n_trials);
ci  = se*2.131;

t   = 2:n_samples;
tt  = [t flip(t)];
cc  = [m - ci flip(m + ci)];

plot(t, m, 'color', d(1,:), 'LineWidth', 2)
patch(tt,cc, d(1,:), 'FaceAlpha', .4, 'EdgeColor', d(1,:))

[a, y_max] = gp_model.discrete_extrema(input_space);
plot([2 n_samples], y_max*[1 1], 'k--', 'LineWidth', 2)

xlabel('# Stimulations/Samples')
ylabel('Output (a.u.)')
set(gca, 'FontSize', 16)

legend({'Random Sweep mean', '± 95% CI',...
    'Optimization-Good Config mean', '± 95% CI',...
    'Optimization-Poor Config mean', '± 95% CI', ...
    'Ground Truth Max'})







