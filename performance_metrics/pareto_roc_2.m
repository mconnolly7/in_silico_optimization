function auc = pareto_roc_2(dom_order_est, dom_order_gt)


for c1 = 1:100
    max_dom_order   = c1-2;

    pos_est                     = dom_order_est  <= max_dom_order;
    neg_est                     = dom_order_est  > max_dom_order;

    pos_gt                      = dom_order_gt == 0;
    neg_gt                      = dom_order_gt > 0;

    pareto_true_pos             = pos_est & pos_gt;
    pareto_true_neg             = neg_est & neg_gt;
    pareto_false_neg            = neg_est & pos_gt;

    recall(c1,1)                = sum(pareto_true_pos) ./ (sum(pareto_true_pos) + sum(pareto_false_neg));
    specificity(c1,1)           = sum(pareto_true_neg) ./ sum(neg_gt);

end

auc = 0;

for c1 = 1:size(specificity,1)-1 % threshold

    x1      = 1-specificity(c1);
    x2      = 1-specificity(c1+1);

    y1      = recall(c1);
    y2      = recall(c1+1);

    c       = x2-x1;
    auc     = auc + c * (y1 + y2)/2;
end

end



