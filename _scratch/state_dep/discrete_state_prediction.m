
T = [timestart(1:384); timestart(385:end) + timestart(384)];

outlier_idx         = zeros(size(T,1),1);
outlier_idx(236)    = 1;

[idx,C,sumd,D]      = kmeans(zscore(Xk0(~outlier_idx,2:30)),2);

%%
plot_matrix(Xk0(~outlier_idx,2:30), T(~outlier_idx), F(2:30))
hold on
plot(T(~outlier_idx), (idx-1)*15+3, 'color', 'r', 'LineWidth', 1)

%%
delta           = 1:4;
theta           = 4:7;
alpha           = 8:15;
beta            = 16:31;
gamma           = 32:50;
high_gamma      = 50:70;

X               = sum(Xk1(:,theta),2);
X1              = X(idx==1);
X2              = X(idx==2);

U               = Stim;
U1              = U(idx==1,:);
U2              = U(idx==2,:);


K_folds = 20;
[~, idx1_mse]   = cross_validate_gp(U1, X1, K_folds);
[~, idx2_mse]   = cross_validate_gp(U2, X2, K_folds);
[~, all_mse]    = cross_validate_gp(Stim, X, K_folds);

X_results  = [idx1_mse/var(X1); idx2_mse/var(X2); all_mse/var(X)];

%%
[a b c] = anova1(X_results',[],'off');
boxplot(X_results', 'Notch', 'on');
xticklabels({'State 1', 'State 2', 'State All'})
ylabel('NMSE');
%%
subplot(1,3,1)
gp1 = gp_object();
gp1.initialize_data(U1,X1);
gp1.plot_mean
gp1.minimize(10);
zlim([0 max(X)])

hold on

subplot(1,3,2)
gp2 = gp_object();
gp2.initialize_data(U2,X2);
gp2.plot_mean
gp2.minimize(10);
zlim([0 max(X)])

subplot(1,3,3)
gp2 = gp_object();
gp2.initialize_data(U,X);
gp2.plot_mean
gp2.minimize(10);

zlim([0 max(X)])