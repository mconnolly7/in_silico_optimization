% load('_scratch/state_dep/temp_results/gamma_model.mat');

state_min   = dynamics_model.gp_model.lower_bound(1);
state_max   = dynamics_model.gp_model.upper_bound(1);

param_min   = dynamics_model.gp_model.lower_bound(2:end);
param_max   = dynamics_model.gp_model.upper_bound(2:end);

tt          = linspace(param_min(1), param_max(1), 100);
tt          = combvec(tt, linspace(param_min(2), param_max(2), 100))';

state_range = linspace(state_min, state_max, 120);

for c1 = 1:size(state_range,2)
    xx      = [repmat(state_range(c1), size(tt,1), 1) tt];
    z(:,c1) = dynamics_model.gp_model.predict(xx);  
    c1
end
%%
z_delta = z - repmat(state_range,size(z,1),1);
z_max = max(max(z_delta));
z_min = min(min(z_delta));

%%
clear t1_max t2_max;

v           = VideoWriter('state_movie_continuous_ca3.avi');
v.FrameRate = 10;
open(v);

figure('Position', [0 0 1400 700])
axis tight manual 
set(gca,'nextplot','replacechildren'); 

for c1 = 1:size(state_range,2)
  
    subplot(2,2,[1 3])
    
    zz  = reshape(z_delta(:,c1),100,100);
    t1  = reshape(tt(:,1), 100, 100);
    t2  = reshape(tt(:,2), 100, 100);
    
    z_zero = zeros(size(t1));
    
    surf(t1,t2,zz)
    hold on
    surf(t1,t2,z_zero, 'FaceAlpha', .5, 'FaceColor', .5*[1 1 1], 'LineStyle', 'none' )
    xlim([min(tt(:,1)) max(tt(:,1))])
    ylim([min(tt(:,2)) max(tt(:,2))])
    zlim([z_min z_max])
    caxis([z_min z_max])
    
    title('Objective Function | State');
    xlabel('Amplitude (mW/m^2)')
    ylabel('Frequency (Hz)')
    zlabel('Gamma Power (V^2/Hz)')
    set(gca, 'FontSize', 18)
    hold off
    
    [~, max_idx]    = max(z(:,c1));
    t1_max(c1)      = tt(max_idx,1);
    t2_max(c1)      = tt(max_idx,2);
        
    subplot(2,2,2)
    plot(state_range(1:c1), t1_max, 'LineWidth', 2, 'Color', zeros(1,3))
    xlim([state_min state_max]);
    ylim([param_min(1) param_max(1)]);
    xticklabels([]);
    ylabel('Optimal Amplitude (mW/mm^2)');
    set(gca,'FontSize', 16);
    
    subplot(2,2,4)
    plot(state_range(1:c1), t2_max, 'LineWidth', 2, 'Color', zeros(1,3))
    xlim([state_min state_max]);
    ylim([param_min(2) param_max(2)]);
    xlabel('Pre-stimultion Gamma State (V^2/Hz)')
    ylabel('Optimal Frequency (Hz)')
    set(gca,'FontSize', 16);

    drawnow
    
    frame = getframe(gcf);
    writeVideo(v,frame);
end

close(v);

% %%
% for c1 = 1:size(state_range,2)
%     subplot(3,4,c1)
% 
%     zz  = reshape(z(:,c1),100,100);
%     t1  = reshape(tt(:,1), 100, 100);
%     t2  = reshape(tt(:,2), 100, 100);
%     
%     surf(t1,t2,zz, 'LineStyle', 'none')
%     zlim([z_min z_max])
% end



