function [X0, X1, U1] = build_dataset(f_band, data_file)
data    = load(data_file);

if ndims(data.Xk0) == 3
    X0     = sum(data.Xk0(:,:,f_band),3);
    X1     = sum(data.Xk1(:,:,f_band),3);
    
else
    X0      = sum(data.Xk0(:,f_band),2);
    X1      = sum(data.Xk1(:,f_band),2);
end

X0     = reshape(X0',[],1);
X1     = reshape(X1',[],1);
U1     = data.Stim;

if ndims(U1) == 3
    freq    = squeeze(U1(:,:,1));
    amp     = squeeze(U1(:,:,2));
    
    amp     = reshape(amp',[],1);
    freq    = reshape(freq',[],1);
    U1     = [amp freq];
end
    U1 = [U1(:,2) U1(:,1)];
end

