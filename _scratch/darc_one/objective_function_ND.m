function objective_val = objective_function_ND(input, objective_means, scale)

n_var               = size(objective_means,2);
objective_sigma     = eye(n_var)*3;
objective_val       = mvnpdf(input, objective_means(1,:), objective_sigma); ... 
%                         + 0.5 * mvnpdf(input, objective_means(2,:), objective_sigma);
                    
objective_val       = objective_val * scale;

end
